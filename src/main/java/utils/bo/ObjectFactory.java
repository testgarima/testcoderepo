package utils.bo;

import javax.xml.bind.annotation.XmlRegistry;

/**
 
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content.
 * The Java representation of XML content can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory methods for each of these are provided in
 * this class.
 */
@XmlRegistry
public class ObjectFactory {

    
    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package:
     * utils.bo
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TestCase }
     *
     * @return TestCase
     */
    public TestCase createTestCase() {
        return new TestCase();
    }

    /**
     * Create an instance of {@link TestScenario }
     *
     * @return TestScenario
     */
    public TestScenario createTestScenario() {
        return new TestScenario();
    }

    /**
     * Create an instance of {@link Param }
     *
     * @return Param
     */
    public Param createParam() {
        return new Param();
    }

    /**
     * Create an instance of {@link Entry }
     *
     * @return Entry
     */
    public Entry createEntry() {
        return new Entry();
    }

    /**
     * Create an instance of {@link TestResult }
     *
     * @return TestResult
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link TestData }
     *
     * @return TestData
     */
    public TestData createTestData() {
        return new TestData();
    }

    /**
     * Create an instance of {@link TestBase }
     *
     * @return TestBase
     */
    public TestBase createTestBase() {
        return new TestBase();
    }

   

}
