package utils.bo;


import java.util.List;
import java.util.stream.Collectors;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "singleTestCase", "testCase"})
@XmlRootElement(name = "testScenario")
public class TestScenario extends TestBase {

    
    /**
     *
     */
    @XmlElement
    protected List<TestCase> testCase;

    /**
     *
     */
    @XmlElement
    private List<String> singleTestCase;

    /**
     *
     */
    @XmlAttribute(required = true)
    protected String name;

    /**
     *
     */
    @XmlAttribute(required = true)
    protected String method;

    /**
     * Gets the value of the testCase property.
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the testCase property.
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getTestCase().add(newItem);
     * </pre>
     * <p>
     * Objects of the following type(s) are allowed in the list {@link TestCase }
     *
     * @return
     */
    public List<TestCase> getTestCases() {
        // the RegEx: getDisabledOnEnrironments="STG;STG-1;STG;DEV;STG" envronment="STG" => only STG is filtered, STG-1 is out
        return testCase.stream()
                .filter(t -> !t.getDisabledOnEnrironments().matches("(^|;)" + runningEnv + "(;|$)"))
                .collect(Collectors.toList());
    }

    /**
     *
     * @param name
     * @return
     */
    public TestCase getTestCaseByName(final String name) {
        return testCase.stream()
                .filter(e -> e.getName().equals(name))
                .filter(t -> !t.getDisabledOnEnrironments().matches("(^|;)" + runningEnv + "(;|$)"))
                .findFirst().get();
    }

    /**
     * Gets the value of the name property.
     *
     * @return possible object is {@link String }
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value allowed object is {@link String }
     */
    @Override
    public void setName(final String value) {
        this.name = value;
    }

    /**
     * Gets the value of the method property.
     *
     * @return possible object is {@link String }
     */
    public String getMethod() {
        return method;
    }

    /**
     * Sets the value of the method property.
     *
     * @param value allowed object is {@link String }
     */
    public void setMethod(final String value) {
        this.method = value;
    }

    /**
     * {@inheritDoc}
     *
     * @return the name of scenario as string
     */
    @Override
    public String toString() {
        return super.getName();
    }

    /**
     * @return the singleTestCase
     */
    public List<String> getSingleTestCase() {
        return singleTestCase;
    }

    /**
     * @param singleTestCase the singleTestCase to set
     */
    public void setSingleTestCase(final List<String> singleTestCase) {
        this.singleTestCase = singleTestCase;
    }

    

   

}
