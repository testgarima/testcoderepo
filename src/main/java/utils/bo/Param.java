package utils.bo;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

/**
 * Java class for anonymous complex type.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"value"})
@XmlRootElement(name = "param")
public class Param {

    /**
     *
     */
    @XmlValue
    private String value;

    /**
     *
     */
    @XmlAttribute(required = false)
    private String environment;

    /**
     *
     */
    @XmlAttribute(required = false)
    private String type;

    /**
     *
     */
    @XmlAttribute(required = true)
    private String key;

    /**
     * When the clear flag is set, this param will not just overwrite same-named params from parent definitions but
     * instead clear (or remove) them.
     */
    @XmlAttribute(required = false)
    private boolean clear;

    /**
     * Gets the value of the value property.
     *
     * @return possible object is {@link String }
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     *
     * @param value allowed object is {@link String }
     */
    public void setValue(final String value) {
        this.value = value;
    }

    /**
     * Gets the value of the key property.
     *
     * @return possible object is {@link String }
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     *
     * @param value allowed object is {@link String }
     */
    public void setKey(final String value) {
        this.key = value;
    }

    /**
     * @return the environment
     */
    public String getEnvironment() {
        return (environment == null) ? "" : environment;
    }

    /**
     * @param environment the environment to set
     */
    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    /**
     *
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     *
     * @param clear
     */
    public void setClear(final boolean clear) {
        this.clear = clear;
    }

    /**
     *
     * @return
     */
    public boolean isClear() {
        return clear;
    }

    /**
     * Get the typed value
     *
     * @return object TypedValue
     * @throws IllegalArgumentException if a method has been passed an illegal or inappropriate argument
     * @throws IllegalAccessException if an application tries to reflectively create an instance
     * @throws InvocationTargetException checkes exception thrown by an invoked method or constructor.
     * @throws SecurityException to indicate a security violation
     * @throws InstantiationException if an instance can't be created
     * @throws ClassNotFoundException if a class is not found
     */
    public Object getTypedValue() throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, SecurityException,
            InstantiationException, ClassNotFoundException {

        final String value = getValue();
        if (value == null || clear) {
            return null;
        } else if (type == null) {
            return value;
        } else {
            final Class<?> typeClass = Class.forName(type);
            try {
                final Method valueOfMethod = typeClass.getMethod("valueOf", String.class);
                return valueOfMethod.invoke(null, value);
            } catch (final NoSuchMethodException e) {
                try {
                    final Constructor<?> ctor = typeClass.getConstructor(String.class);
                    return ctor.newInstance(value);
                } catch (final NoSuchMethodException e1) {
                    return value;
                }
            }
        }
    }

}
