package utils.bo;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {  "results" })
@XmlRootElement(name = "testCase")
public class TestCase extends TestBase {

    /**
     *
     */
    protected static final Logger LOG = LoggerFactory.getLogger(TestBase.class);

    /**
     *
     */
    @XmlAttribute(required = true)
    protected String name;

    /**
     *
     */
    @XmlAttribute
    protected String disabledOnEnrironments;

    /**
     *
     */
    @XmlAttribute
    private boolean isLiveTest = false;

   

    
    /**
     *
     */
    @XmlAttribute(required = false)
    private String scenarioName;

    /**
     *
     */
    @XmlElement(name = "result")
    private List<Result> results = new ArrayList<>();

    /**
     * Sets the value of the devEnabled property.
     *
     * @param value allowed object is {@link Boolean }
     */
    public void setDisabledStagings(final String value) {
        this.disabledOnEnrironments = value;
    }

    /**
     * Gets the value of the devEnabled property.
     *
     * @return possible object is {@link Boolean }
     */
    public String getDisabledOnEnrironments() {
        return disabledOnEnrironments != null ? disabledOnEnrironments : "";
    }

    /**
     * Gets the value of the name property.
     *
     * @return possible object is {@link String }
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value allowed object is {@link String }
     */
    @Override
    public void setName(final String value) {
        this.name = value;
    }

    /**
     * Returns the name and the list of known issues, if specified in the xml.
     *
     * @return name and the list of known issues, if specified in the xml.
     */
    @Override
    public String toString() {
        String s = name;
        if (issue != null && issue.isEmpty() == false) {
            s += " * Known Issues: " + issue;
        }
        return s;
    }

    //
    //    /**
    //     * Returns the Result as Map. The Key of the Map is the ID of the Result.
    //     *
    //     * @return Result as Map.
    //     */
    //    public Map<String, Result> getExpectedResultAsMap() {
    //        final Map<String, Result> rm = new HashMap<String, Result>();
    //        if (getExpectedResult() != null && !getExpectedResult().isEmpty()) {
    //            for (final Result testResult : getExpectedResult()) {
    //                rm.put(testResult.getId(), testResult);
    //            }
    //        }
    //        return rm;
    //    }
    //
    //    /**
    //     * Returns the expected Result at the specified position.
    //     *
    //     * @param index - position
    //     * @return Result
    //     */
    //    public Result getResult(final int index) {
    //        if (getExpectedResult().size() == 0) {
    //            return null;
    //        }
    //        return getExpectedResult().get(index);
    //    }
    //
    //    /**
    //     * Sets the value of the filter property.
    //     *
    //     * @param value allowed object is {@link Filter }
    //     */
    //    public void setFilter(final Filter value) {
    //        this.filter = value;
    //    }
    //
    //    /**
    //     * Returns the filter values as List
    //     *
    //     * @return List of the filter values.
    //     */
    //    public List<ExtParams> getFilterValues() {
    //        final List<ExtParams> extParams = new ArrayList<ExtParams>();
    //        if (filter != null && filter.getElem() != null) {
    //            for (final Elem e : filter.getElem()) {
    //                final Map<String, String> args = new HashMap<String, String>();
    //                for (final Arg a : e.getArg()) {
    //                    String v = a.getContent().trim();
    //                    v = v.replaceAll("\\n", "");
    //                    v = v.replaceAll("\\t", "");
    //                    args.put(a.getKey(), v);
    //                }
    //                final ExtParams ep = new ExtParams();
    //                ep.setArgs(args);
    //                extParams.add(ep);
    //            }
    //        }
    //        return extParams;
    //    }
    //
    //    /**
    //     * Returns the filter values as List.
    //     *
    //     * @param staging the used staging
    //     * @param allowedStagings all stagings allowed
    //     * @return List of the filter values with staging.
    //     */
    //    public List<ExtParams> getFilterValues(final String staging, final String allowedStagings) {
    //        final List<ExtParams> extParams = new ArrayList<ExtParams>();
    //        if (filter != null && filter.getElem() != null) {
    //            for (final Elem e : filter.getElem()) {
    //                Map<String, String> args = new HashMap<String, String>();
    //                final List<String> stagingDependentKeys = new ArrayList<String>();
    //                final List<String> movedEntries = new ArrayList<String>();
    //                for (final Arg a : e.getArg()) {
    //                    String v = a.getContent().trim();
    //                    v = v.replaceAll("\\n", "");
    //                    v = v.replaceAll("\\t", "");
    //
    //                    if (a.getKey().contains(staging)) {
    //
    //                        final String newKey = a.getKey().replaceFirst(staging, "");
    //                        args.put(newKey, v);
    //                        stagingDependentKeys.add(newKey);
    //                        movedEntries.add(newKey);
    //                    } else {
    //                        if (!movedEntries.contains(a.getKey())) {
    //                            args.put(a.getKey(), v);
    //                        }
    //                    }
    //                }
    //                args = removeStagingDependentEntries(args, stagingDependentKeys, allowedStagings);
    //                final ExtParams ep = new ExtParams();
    //                ep.setArgs(args);
    //                extParams.add(ep);
    //            }
    //        }
    //        return extParams;
    //    }

    //    /**
    //     *
    //     * @param unfilteredEntries
    //     * @param stagingDependentKeys
    //     * @param allowedStagingsString
    //     * @return
    //     */
    //    private Map<String, String> removeStagingDependentEntries(final Map<String, String> unfilteredEntries, final List<String> stagingDependentKeys,
    //            final String allowedStagingsString) {
    //        final Map<String, String> filteredEntries = new HashMap<>();
    //        String allowedStagings[] = {""};
    //
    //        for (final String key : unfilteredEntries.keySet()) {
    //            boolean keep = true;
    //
    //            if (allowedStagingsString != null && !allowedStagingsString.isEmpty()) {
    //                allowedStagings = allowedStagingsString.split(";");
    //            } else {
    //                LOG.warn("allowedStagings is empty or not set. Please set it in the TestData xml file, like : <param key=\"allowedStagings\">DEV;STG;LIV</param>");
    //            }
    //
    //            for (final String stagingDependentKey : stagingDependentKeys) {
    //                for (final String stagingPrefix : allowedStagings) {
    //                    if (key.startsWith(stagingPrefix) && !key.equals(stagingDependentKey)) {
    //                        keep = false;
    //                        break;
    //                    }
    //                }
    //            }
    //
    //            if (keep) {
    //                filteredEntries.put(key, unfilteredEntries.get(key));
    //            }
    //        }
    //        return filteredEntries;
    //    }

    /**
     * @return the isLiveTest
     */
    public boolean isLiveTest() {
        return isLiveTest;
    }

    /**
     * @param isLiveTest the isLiveTest to set
     */
    public void setIsLiveTest(final boolean isLiveTest) {
        this.isLiveTest = isLiveTest;
    }

   
  
    /**
     *
     * @param testScenarioName
     */
    public void setScenarioName(final String testScenarioName) {
        this.scenarioName = testScenarioName;
    }

    /**
     *
     * @return
     */
    public String getSenarioName() {
        return this.scenarioName;
    }

    /**
     * Gets the value of the expectedResults property.
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the expectedResults property.
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getResult().add(newItem);
     * </pre>
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Result }
     *
     * @return
     */
    public List<Result> getResults() {
        return results.stream()
                .filter(r -> r.getEnvironment().equals(runningEnv) || r.getEnvironment().equals(""))
                .sorted((r1, r2) -> r2.getEnvironment().compareTo(r1.getEnvironment()))
                .peek(r -> r.setRunningEnv(runningEnv))
                .collect(Collectors.toList());
    }

    /**
     * returns the fist Result which matches the envrironment
     *
     * @return
     */
    public Result getResult() {
        return results.stream()
                .filter(r -> r.getEnvironment().equals(runningEnv) || r.getEnvironment().equals(""))
                .sorted((r1, r2) -> r2.getEnvironment().compareTo(r1.getEnvironment()))
                .peek(r -> r.setRunningEnv(runningEnv))
                .findFirst().orElse(null);
    }

    /**
     * Returns the Result for the given type or null.
     *
     * @param testResultType Result type
     * @return List of Results
     */
    public Result getResultByType(final String testResultType) {
        return results.stream()
                .filter(e -> e.getType().equals(testResultType))
                .filter(Objects::nonNull)
                .filter(r -> r.getEnvironment().equals(runningEnv) || r.getEnvironment().equals(""))
                .sorted((r1, r2) -> r2.getEnvironment().compareTo(r1.getEnvironment()))
                .peek(r -> r.setRunningEnv(runningEnv))
                .findFirst().orElse(null);
    }

    /**
     * @param results
     */
    public void setResults(final List<Result> results) {
        this.results = results;
    }
}
