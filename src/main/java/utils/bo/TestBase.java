package utils.bo;


import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.apache.commons.lang3.StringUtils;

/**
 * Provides base methods for the test classes, like getting parameters.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "testBase", propOrder = { "runningEnv", "requiredVersion", "issue", "comment", "param", "extParam", "allowedEnvrironments" })
@XmlSeeAlso({ TestData.class, TestScenario.class, TestCase.class })
public class TestBase {

    /**
     * The name of this element.
     */
    @XmlTransient
    protected String name;

    /**
     *
     */
    protected String requiredVersion;

    /**
     * Maps to <issue/> elements. Mapping is done by name, so don't rename it!
     */
    protected List<String> issue;

    /**
     * Maps to <comment/> elements. Mapping is done by name, so don't rename it!
     */
    protected String comment;

    /**
     * Maps to <param/> elements. Mapping is done by name, so don't rename it!
     */
    protected List<Param> param;

    /**
     *
     */
    protected List<ExtParam> extParam;

    /**
     * the test environment fe. staging level (DEV,STG,LIVE,...)
     */
    @XmlElement(name = "environment")
    protected String runningEnv;

    /**
     *
     */
    @XmlTransient
    private TestBase parent;

    /**
     *
     */
    @XmlTransient
    private String testClassName;

    /**
     *
     */
    @XmlElement
    protected String allowedEnvrironments;

    /**
     * Gets the value of the issue property.
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the issue property.
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getIssue().add(newItem);
     * </pre>
     * <p>
     * Objects of the following type(s) are allowed in the list {@link String }
     *
     * @return list of issues as {@link String }
     */
    public List<String> getIssues() {
        if (issue == null) {
            issue = new ArrayList<>();
        }
        return issue;
    }

    /**
     * Get known issues
     *
     * @return known issues as {@link String }
     */
    public String getKnownIssues() {
        return issue.toString();
    }

    /**
     * Get the Comment
     *
     * @return comment as {@link String }
     */
    public String getComment() {
        return comment;
    }

    /**
     * Gets the value of the param property.
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the param property.
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getParam().add(newItem);
     * </pre>
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Param }
     *
     * @return List of Params
     */
    public List<Param> getParam() {
        if (param == null) {
            param = new ArrayList<>();
        }
        return param;
    }

    /**
     * Gets the value of the extParam property.
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the extParam property.
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getExtParam().add(newItem);
     * </pre>
     * <p>
     * Objects of the following type(s) are allowed in the list {@link ExtParamType }
     *
     * @return List of ExtParams
     */
    public List<ExtParam> getExtParams() {
        return getExtParam() != null ? extParam : new ArrayList<>();
    }

    /**
     * returns a list of extParam depending on runningEnv and given type
     *
     * @param type type of ExtParam
     * @return list of ExtParam, may be empty if not found
     */
    public List<ExtParam> getExtParams(final String type) {

        final List<ExtParam> extParams = getExtParam().stream()
                .filter(e -> e.getEnvironment().equals(runningEnv) || e.getEnvironment().equals(""))
                .filter(e -> e.type.equals(type))
                .peek(e -> e.setRunningEnv(runningEnv))
                .peek(e -> e.setParentTestBase(this))
                .collect(Collectors.toList());

        if (!extParams.isEmpty()) {
            return extParams;
        } else if (parent != null) {
            return parent.getExtParams(type);
        }
        return extParams;
    }

    /**
     * returns an extParam depending on runningEnv and given type and name
     *
     * @param type type of ExtParam
     * @param name name of ExtParam
     * @return ExtParam, may be null if not found
     */
    public ExtParam getExtParam(final String type, final String name) {

        final ExtParam extParam = getExtParam().stream()
                .filter(e -> e.getEnvironment().equals(runningEnv) || e.getEnvironment().equals(""))
                .filter(e -> e.type.equals(type))
                .filter(e -> e.name.equals(name))
                .sorted((e1, e2) -> e2.getEnvironment().compareTo(e1.getEnvironment()))
                .peek(e -> e.setRunningEnv(runningEnv))
                .peek(e -> e.setParentTestBase(this))
                .findFirst().orElse(null);

        if (extParam != null) {
            return extParam;
        } else if (parent != null) {
            return parent.getExtParam(type, name);
        }
        return null;
    }

    /**
     * Returns the parameter assigned to the test case. If the parameter is not configured for the test case the
     * parameter will be read from the test scenario. Whitespace If you have specified a runningEnv property this
     * property will be prefixed to the key name. For example:<br>
     * <br>
     * {@code <!--     <param key="runningEnv">DEV</param> -->}<br>
     * {@code<param key="runningEnv">STG</param>}<br>
     * <br>
     * {@code<param key="DEV.Var1">http://server.DEV.com</param>}<br>
     * {@code<param key="STG.Var1">http://server.STG.com</param>}<br>
     * {@code<test-scenario name="MyTest" method="testMyTest">}<br>
     * {@code<test-case name="mySampleTest">}<br>
     * {@code<comment>shows the usage of runningEnv property</comment>}<br>
     * {@code<param key="STG.testProperty1">customer STG</param>}<br>
     * {@code<param key="DEV.testProperty1">customer DEV</param>}<br>
     * {@code</test-case>}<br>
     * {@code</test-scenario>}<br>
     * <br>
     * you can also use system parameter:<br>
     * <br>
     * {@code -Dqa.runningEnv=DEV} In the tests you only have to call for:<br>
     * <br>
     * {@code testCase.getParam("Var1"));} => http://server.STG.com<br>
     * {@code testCase.getParam("testProperty1"));} => customer STG<br>
     * <br>
     * This is because runningEnv=STG is enabeld!
     *
     * @param key parameter key
     * @return Parameter or <code>null</code> if no parameter is assigned to the key.
     */
    public String getParam(final String key) {
        if (param != null) {
            final Param get = param.stream()
                    .filter(p -> p.getKey().equals(key))
                    .filter(p -> p.getEnvironment().equals(getRunningEnv()) || p.getEnvironment().equals(""))
                    .sorted((p1, p2) -> p2.getEnvironment().compareTo(p1.getEnvironment()))
                    .findFirst().orElse(null);
            if (get != null) {
                return get.getValue();
            }
        }
        if (parent != null) {
            return parent.getParam(key);
        }
        return null;
    }

    /**
     * Get typed param
     *
     * @param key the key
     * @return typd param object
     * @throws IllegalArgumentException if a method has been passed an illegal or inappropriate argument
     * @throws SecurityException to indicate a security violation
     * @throws IllegalAccessException if an application tries to reflectively create an instance
     * @throws InvocationTargetException checkes exception thrown by an invoked method or constructor.
     * @throws InstantiationException if an instance can't be created
     * @throws ClassNotFoundException if a class is not found
     */
    public Object getTypedParam(final String key) throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException,
    InstantiationException, ClassNotFoundException {
        if (param != null) {
            for (final Param p : param) {
                if (p.getKey().equals(key)) {
                    return p.getTypedValue();
                }
            }
        }
        if (parent != null) {
            return parent.getTypedParam(key);
        }
        return null;
    }

    /**
     * see {@link TestCase#getParam(String)}
     *
     * @param key the key
     * @return the integer param
     */
    public int getIntParam(final String key) {
        return Integer.valueOf(getParam(key));
    }

    /**
     * see {@link TestCase#getParam(String)}
     *
     * @param key the key
     * @return the byte param
     */
    public Byte getByteParam(final String key) {
        return Byte.valueOf(getParam(key));
    }

    /**
     * see {@link TestCase#getParam(String)}
     *
     * @param key the key
     * @return the short param
     */
    public Short getShortParam(final String key) {
        return Short.valueOf(getParam(key));
    }

    /**
     * Gets the {@link Param} value as a {@link Integer} or null if there no param with this key.
     *
     * @param key the key
     * @return the integer param or null
     */
    public Integer getIntegerParamOrNull(final String key) {
        final String value = getParam(key);
        if (StringUtils.isEmpty(value)) {
            return null;
        } else {
            return Integer.valueOf(value);
        }
    }

    /**
     * see {@link TestCase#getParam(String)}
     *
     * @param key the key
     * @return the big integer param
     */
    public BigInteger getBigIntParam(final String key) {
        return BigInteger.valueOf(getLongParam(key));
    }

    /**
     * Gets the {@link Param} value as a {@link BigInteger} or null if there no param with this key.
     *
     * @param key the key
     * @return the bigInteger param or null
     */
    public BigInteger getBigIntegerParamOrNull(final String key) {
        final String value = getParam(key);
        if (StringUtils.isEmpty(value)) {
            return null;
        } else {
            return new BigInteger(value);
        }
    }

    /**
     * Gets the {@link Param} value as a {@link BigDecimal} or null if there no param with this key.
     *
     * @param key the key
     * @return the bigDecimal param or null
     */
    public BigDecimal getBigDecimalParamOrNull(final String key) {
        final String value = getParam(key);
        if (StringUtils.isEmpty(value)) {
            return null;
        } else {
            return new BigDecimal(value);
        }
    }

    /**
     * see {@link TestCase#getParam(String)}
     *
     * @param key the key
     * @return the long param
     */
    public long getLongParam(final String key) {
        return Long.valueOf(getParam(key));
    }

    /**
     * Gets the {@link Param} value as a {@link Long} or null if there no param with this key.
     *
     * @param key the key
     * @return the long param or null
     */
    public Long getLongParamOrNull(final String key) {
        return getLongParamOrDefault(key, null);
    }

    /**
     * Gets the {@link Param} value as a {@link Long} or the specified default if there no param with this key.
     *
     * @param key the key
     * @param defaultValue the default value
     * @return the long param or default
     */
    public Long getLongParamOrDefault(final String key, final Long defaultValue) {
        final String value = getParam(key);
        if (StringUtils.isEmpty(value)) {
            return defaultValue;
        } else {
            return Long.valueOf(value);
        }
    }

    /**
     * see {@link TestCase#getParam(String)}
     *
     * @param key the key
     * @return the double param
     */
    public double getDoubleParam(final String key) {
        return Double.valueOf(getParam(key));
    }

    /**
     * Gets the {@link Param} value as a {@link Double} or null if there no param with this key.
     *
     * @param key the key
     * @return the double param or null
     */
    public Double getDoubleParamOrNull(final String key) {
        final String value = getParam(key);
        if (StringUtils.isEmpty(value)) {
            return null;
        } else {
            return Double.valueOf(value);
        }
    }

    /**
     * see {@link TestCase#getParam(String)}
     *
     * @param key the key
     * @return the boolean param
     */
    public boolean getBooleanParam(final String key) {
        return Boolean.valueOf(getParam(key));
    }

    /**
     * Gets the {@link Param} value as a boolean or null if there no param with this key.
     *
     * @param key the key
     * @return the boolean param or null
     */
    public Boolean getBooleanParamOrNull(final String key) {
        return getBooleanParamOrDefault(key, null);
    }

    /**
     * Gets the {@link Param} value as a boolean the specified default if there no param with this key.
     *
     * @param key the key
     * @param defaultValue the default value
     * @return the boolean param or default
     */
    public Boolean getBooleanParamOrDefault(final String key, final Boolean defaultValue) {
        final String value = getParam(key);
        if (StringUtils.isEmpty(value)) {
            return defaultValue;
        } else {
            return Boolean.valueOf(value);
        }
    }

    /**
     * Gets the {@link Param} value as a short or null if there no param with this key.
     *
     * @param key the key
     * @return the short param or null
     */
    public Short getShortParamOrNull(final String key) {
        return getShortParamOrDefault(key, null);
    }

    /**
     * Gets the {@link Param} value as a short the specified default if there no param with this key.
     *
     * @param key the key
     * @param defaultValue the default value
     * @return the short param or default
     */
    public Short getShortParamOrDefault(final String key, final Short defaultValue) {
        final String value = getParam(key);
        if (StringUtils.isEmpty(value)) {
            return defaultValue;
        } else {
            return Short.valueOf(value);
        }
    }

    /**
     * Returns the parameter assigned to the test case. If the parameter is not configured for the test case the
     * parameter will be read from the test scenario. When no value is found the default value is returned.
     *
     * @param key parameter key
     * @param def Default value, when no value is specified in the config for the key
     * @return Parameter or <code>null</code> if no parameter is assigned to the key.
     */
    public String getParam(final String key, final String def) {
        final String ret = getParam(key);
        if (ret == null) {
            return def;
        }
        return ret;
    }

    /**
     * Get parent testbase
     *
     * @return parent testbase
     */
    public TestBase getParent() {
        return parent;
    }

    /**
     * Set parent testbase
     *
     * @param parent
     */
    public void setParent(final TestBase parent) {
        this.parent = parent;
    }

    /**
     * @return the runningEnv
     */
    public String getRunningEnv() {
        return runningEnv;
    }

    /**
     * NOTE: if you set here the runningEnv all inherits (TestScenario, pp) don't get it!
     *
     * @param runningEnv the runningEnv to set
     */
    public void setRunningEnv(final String runningEnv) {
        this.runningEnv = runningEnv;
    }

    /**
     * @return the allowed stagings
     */
    public String getAllowedEnvironmens() {
        return allowedEnvrironments;
    }

    /**
     * @param allowedEnvrironments the allowed stagings to set
     */
    public void setAllowedEnvrionments(final String allowedEnvrironments) {
        this.allowedEnvrironments = allowedEnvrironments;
    }

    /**
     * Gets the required version since which the test is expected to work.
     *
     * @return the required version
     */
    public String getRequiredVersion() {
        return requiredVersion;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the ClassName of the test
     */
    public String getTestClassName() {
        if (testClassName == null) {
            if (getParent() != null) {
                this.testClassName = getParent().getTestClassName();
            }
        }
        return testClassName;
    }

    /**
     * @param testClassName the testClassName to set
     */
    public void setTestClassName(final String testClassName) {
        this.testClassName = testClassName;
    }

    /**
     * @return the extParam
     */
    public List<ExtParam> getExtParam() {
        if (extParam == null) {
            extParam = new ArrayList<>();
        }
        return extParam;
    }

    /**
     * @param extParam the extParam to set
     */
    public void setExtParam(final List<ExtParam> extParam) {
        this.extParam = extParam;
    }

}
