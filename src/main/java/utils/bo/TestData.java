package utils.bo;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.sax.SAXSource;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"testScenario" })
@XmlRootElement(name = "testData")
public class TestData extends TestBase {

    /**
     *
     */
    @XmlTransient
    protected List<TestClass> testClass;

    // @XmlAttribute
    // private String name;

    /**
     *
     */
    @XmlElement(name = "testScenario")
    protected List<TestScenario> testScenario;

   
    /**
     * get TestClasses
     *
     * @return List of testclasses
     */
    public List<TestClass> getTestClasses() {
        if (testClass == null) {
            testClass = new ArrayList<>();
        }
        return testClass;
    }

    /**
     * Get TestClass for classname
     *
     * @param className the class name
     * @return the testclass
     */
    public TestClass getTestClass(final String className) {
        TestClass currentTestClass = null;
        if (testClass != null) {
            for (final TestClass tc : testClass) {
                if (tc.getClazz().equals(className)) {
                    currentTestClass = tc;
                    break;
                }
            }
        }
        return currentTestClass;

    }
    /**
     * Gets all testScenarios.
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the testScenario property.
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getTestScenario().add(newItem);
     * </pre>
     * <p>
     * Objects of the following type(s) are allowed in the list {@link TestScenario }
     *
     * @return
     */
    public List<TestScenario> getTestScenarios() {
        if (testScenario == null) {
            testScenario = new ArrayList<>();
        }
        return testScenario;
    }

    /**
     * Returns the {@link TestScenario} assigned to the method name.
     *
     * @param methodName The name of the test method
     * @return TestScenario assigned to the name of <code>null</code> if no TestScenario is assigned.
     */
    public TestScenario getTestScenario(final String methodName) {
        TestScenario scenario = null;
        for (final TestScenario ts : testScenario) {
            if (ts.getMethod().equals(methodName)) {
                scenario = ts;
                break;
            }
        }
        return scenario;
    }

    /**
     * Returns an instance of TestData. The data is read and created via Castor from the specified xml file. It also
     * handles staging prefixes. This could be either in the xml file like <br> null null
     * {@code <param key="staging">STG</param>}<br>
     * or as system parameter like<br> {@code -Dqa.staging=STG }
     *
     * @param fileName - XML file containing the TestData
     * @return Instance of TestData
     * @throws Exception When the the xml could not be parsed into TestData
     */
    public static TestData getInstance(final String fileName) throws Exception {

        final JAXBContext jc = JAXBContext.newInstance("utils.bo");
        final Unmarshaller unmarshaller = jc.createUnmarshaller();

        final ClassLoader cl = Thread.currentThread().getContextClassLoader();
        final InputStream stream = cl.getResourceAsStream(fileName);

        if (stream == null) {
            throw new FileNotFoundException("File " + fileName + " can not be found in the classpath!");
        }

        final SAXParserFactory spf = SAXParserFactory.newInstance();

        spf.setXIncludeAware(true);
        spf.setNamespaceAware(true);
        spf.setValidating(true); // Not required for JAXB/XInclude

        final XMLReader xr = spf.newSAXParser().getXMLReader();
        final SAXSource source = new SAXSource(xr, new InputSource(stream));

        final TestData td = (TestData) unmarshaller.unmarshal(source);

        final String environment = TestData.getStaging(td);
        td.setRunningEnv(environment);

        // We need the parent, so that the params can be retrieved from the superior objects
        for (final TestScenario ts : td.getTestScenarios()) {
            ts.setParent(td);
            ts.setRunningEnv(environment);
            for (final TestCase tc : ts.getTestCases()) {
                tc.setParent(ts);
                tc.setRunningEnv(environment);
            }
        }


        return td;
    }

    /**
     * returns any staging prefixes. This could be either in the xml file like <br>
     * null null null {@code
     *  <param key="staging">STG</param>
     * } <br>
     * or as properties file in $HOME/.qatools/staging.properties<br>
     * null null null null {@code
     * current=STG
     * }<br>
     * or as system parameter like<br>
     * null null null null {@code
     * -Dqa.staging=STG
     * }
     *
     * @param td
     * @return assembled sting like 'STG.'
     */
    private static String getStaging(final TestData td) {

        String staging = td.getRunningEnv();

        // hook to be able to run Unit-Tests for Faynos itself....
        // if ("TestDataTemplate".equals(td.getName())) {
        // return staging;
        // }

        if (System.getProperty("qa.staging") != null) {
            staging = System.getProperty("qa.staging");

        } else if (System.getProperty("user.dir") != null) {
            final String fileName = System.getProperty("user.home") + File.separator + ".qatools" + File.separator + "staging.properties";
            final File propFile = new File(fileName);
            if (propFile.isFile()) {
                final Properties prop = new Properties();

                try {
                    try (FileInputStream fileInputStream = new FileInputStream(fileName)) {
                        prop.load(fileInputStream);
                        final String current = prop.getProperty("current");
                        if (current != null && !current.isEmpty()) {
                            staging = current;
                        }
                    }
                } catch (final IOException ex) {
                    Logger.getLogger(TestData.class.getName()).log(Level.SEVERE, "could not read " + fileName, ex);
                }
            }

        }

        return staging;
    }

    /**
     * @return the name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    @Override
    public void setName(final String name) {
        this.name = name;
    }

   

   

}
