package utils.bo;

import static utils.bo.Result.log;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "extEntry", propOrder = {"entry", "extEntry"})
@XmlRootElement(name = "result")
public class ExtEntry {

    /**
     *
     */
    private static final String SPLIT_DELIMITER = ",";

    /**
     *
     */
    protected List<Entry> entry;

    /**
     *
     */
    @XmlElement
    protected List<ExtEntry> extEntry;

    /**
     *
     */
    @XmlAttribute(required = true)
    protected String type;

    /**
     *
     */
    @XmlAttribute
    protected String name;

    /**
     *
     */
    @XmlAttribute
    private String environment = "";

    /**
     * this property stores the current requestet environement (DEV, STG, LIVE,....)
     */
    @XmlTransient
    private String runningEnv;

    /**
     *
     */
    @XmlTransient
    private ExtEntry parent;

    /**
     *
     */
    @XmlTransient
    private Result parentTestResult;

    /**
     * Gets the value of the param property.
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the param property.
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getEntry().add(newItem);
     * </pre>
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Entry }
     *
     * @return list of entries
     */
    public List<Entry> getEntries() {
        if (entry == null) {
            entry = new ArrayList<>();
        }
        return this.entry;
    }

    /**
     * Converts a comma separated string into a List of Long's
     *
     * @param key parameter key
     * @return List with ids
     */
    public List<Long> getEntriesAsListOfLongs(final String key) {
        final List<Long> longIds = new ArrayList<>();
        final String ids = getEntry(key);
        if (ids != null) {
            for (final String s : ids.split(SPLIT_DELIMITER)) {
                longIds.add(Long.valueOf(s));
            }
        }
        return longIds;
    }

    /**
     * Converts a comma separated string into a List of Int's
     *
     * @param key parameter key
     * @return List with ids
     */
    public List<Integer> getEntryAsListOfInts(final String key) {
        final List<Integer> intIds = new ArrayList<>();
        final String ids = getEntry(key);
        if (ids != null) {
            for (final String s : ids.split(SPLIT_DELIMITER)) {
                intIds.add(Integer.valueOf(s));
            }
        }
        return intIds;
    }

    /**
     * Converts a comma separated string into a List of String's
     *
     * @param key parameter key
     * @return List with ids
     */
    public List<String> getEntryAsListOfStrings(final String key) {
        final List<String> strings = new ArrayList<>();
        final String ids = getEntry(key);
        if (ids != null) {
            for (final String s : ids.split(SPLIT_DELIMITER)) {
                strings.add(s);
            }
        }

        return strings;
    }

    /**
     * Gets the value of the extEntry property.
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the extEntry property.
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getExtEntry().add(newItem);
     * </pre>
     * <p>
     * Objects of the following type(s) are allowed in the list {@link ExtEntry }
     *
     * @return List of extended entries
     */
    public List<ExtEntry> getExtEntry() {
        if (extEntry == null) {
            extEntry = new ArrayList<>();
        }
        return this.extEntry;
    }

    /**
     * returns a list of ext-param-container depending on environment and given type
     *
     * @param type type of the ext entry
     * @return List of extended entries
     */
    public List<ExtEntry> getExtEntries(final String type) {
        return getExtEntries(type, null);
    }

    /**
     * returns a list of ext-entry-container depending on environment and given type
     *
     * @param type type of the ext entry
     * @param name name of the ext entry
     * @return List of extended entries
     */
    public List<ExtEntry> getExtEntries(final String type, final String name) {
        final List<ExtEntry> extEntries = new ArrayList<>();
        if (extEntry != null) {
            final Iterator<ExtEntry> iterator = extEntry.iterator();
            while (iterator.hasNext()) {
                final ExtEntry extEntryType = iterator.next();
                if (extEntryType.type.equals(type) && (extEntryType.getEnvironment() == null || runningEnv.indexOf(extEntryType.getEnvironment()) != -1)) {
                    if (name == null || name.equals(extEntryType.name)) {
                        extEntryType.setRunningEnv(runningEnv);
                        extEntryType.setParent(this);
                        extEntries.add(extEntryType);
                    }
                }
            }
        }
        return extEntries;
    }

    /**
     * Returns the entries as Map.
     *
     * @param staging environment of the requested entries
     * @param allowedStagings all allowed stagings (needed to filter the actual used environment)
     * @return TestResult entries as Map with environment.
     */
    public Map<String, String> getEntries(final String staging, final String allowedStagings) {
        Map<String, String> entries = new HashMap<>();
        final List<String> movedEntries = new ArrayList<>();
        final List<String> stagingDependentKeys = new ArrayList<>();

        for (final Entry e : entry) {
            String v = e.getContent().trim();
            v = v.replaceAll("\\n", "");
            v = v.replaceAll("\\t", "");
            // System.out.println("e.getKey(): " + e.getKey());
            if (e.getKey().startsWith(staging)) {
                final String newKey = e.getKey().replaceFirst(staging, "");
                entries.put(newKey, v);
                stagingDependentKeys.add(newKey);
                movedEntries.add(newKey);
            } else {
                if (!movedEntries.contains(e.getKey())) {
                    entries.put(e.getKey(), v);
                }
            }
        }

        entries = removeStagingDependentEntries(entries, stagingDependentKeys, allowedStagings);
        return entries;
    }

    /**
     *
     * @param unfilteredEntries
     * @param stagingDependentKeys
     * @param allowedStagingsString
     * @return
     */
    private Map<String, String> removeStagingDependentEntries(final Map<String, String> unfilteredEntries, final List<String> stagingDependentKeys,
            final String allowedStagingsString) {
        final Map<String, String> filteredEntries = new HashMap<>();
        String allowedStagings[] = {""};

        for (final String key : unfilteredEntries.keySet()) {
            boolean keep = true;

            if (allowedStagingsString != null && allowedStagingsString.length() > 0) {
                allowedStagings = allowedStagingsString.split(";");
            } else {
                log.warn("allowedStagings is empty or not set. Please set it in the TestData xml  file, like : <param key=\"allowedStagings\">DEV;STG;LIV</param>");
            }

            for (final String stagingDependentKey : stagingDependentKeys) {
                for (final String stagingPrefix : allowedStagings) {
                    if (key.startsWith(stagingPrefix + ".") && !key.equals(stagingDependentKey)) {
                        keep = false;
                        break;
                    }
                }
            }

            if (keep) {
                filteredEntries.put(key, unfilteredEntries.get(key));
            }
        }
        return filteredEntries;
    }

    /**
     * see {@link TestCase#getEntry(String)}
     *
     * @param key key of the Entry
     * @return Integer value for the requested key
     */
    public int getIntEntry(final String key) {
        return Integer.valueOf(getEntry(key));
    }

    /**
     * see {@link TestCase#getEntry(String)}
     *
     * @param key key of the Entry
     * @return BigInteger value for the requested key
     */
    public BigInteger getBigIntEntry(final String key) {
        return BigInteger.valueOf(getLongEntry(key));
    }

    /**
     * see {@link TestCase#getEntry(String)}
     *
     * @param key key of the Entry
     * @return Long value for the requested key
     */
    public long getLongEntry(final String key) {
        return Long.valueOf(getEntry(key));
    }

    /**
     * see {@link TestCase#getEntry(String)}
     *
     * @param key key of the Entry
     * @return Double value for the requested key
     */
    public double getDoubleEntry(final String key) {
        return Double.valueOf(getEntry(key));
    }

    /**
     * see {@link TestCase#getEntry(String)}
     *
     * @param key key of the Entry
     * @return Boolean value for the requested key
     */
    public boolean getBooleanEntry(final String key) {
        return Boolean.valueOf(getEntry(key));
    }

    /**
     * see {@link TestCase#getEntry(String)}
     *
     * @param key key of the Entry
     * @return BigDecimal value for the requested key
     */
    public BigDecimal getBigDecimalEntry(final String key) {
        return new BigDecimal(getEntry(key));
    }

    /**
     * see {@link TestCase#getEntry(String)}
     *
     * @param key key of the Entry
     * @return Byte value for the requested key
     */
    public Byte getByteEntry(final String key) {
        return Byte.valueOf(getEntry(key));
    }

    /**
     * see {@link TestCase#getEntry(String)}
     *
     * @param key key of the Entry
     * @return Short value for the requested key
     */
    public Short getShortEntry(final String key) {
        return Short.valueOf(getEntry(key));
    }

    /**
     * Returns the value of the type-property.
     *
     * @return possible object is {@link String }
     */
    public String getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     *
     * @param value allowed object is {@link String }
     */
    public void setType(final String value) {
        this.type = value;
    }

    /**
     * Returns the value of the name-property.
     *
     * @return possible object is {@link String }
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name-property.
     *
     * @param value allowed object is {@link String }
     */
    public void setName(final String value) {
        this.name = value;
    }

    /**
     * @return the environment
     */
    public String getEnvironment() {
        return environment;
    }

    /**
     * @param environment the environment to set
     */
    public void setEnvironment(final String environment) {
        this.environment = environment;
    }

    /**
     * @return the environment
     */
    public String getRunningEnv() {
        return runningEnv;
    }

    /**
     * @param environment the environment to set
     */
    public void setRunningEnv(final String environment) {
        this.runningEnv = environment;
    }

    /**
     * @param key the key
     * @return entry for the key
     */
    public String getEntry(final String key) {
        if (entry != null) {
            String value = null;
            final String stage = getRunningEnv() + key;
            for (final Entry p : entry) {
                if (p.getKey().equals(stage)) {
                    if (p.getContent() == null) {
                        return null;
                    } else {
                        return p.getContent().trim();
                    }
                } else if (p.getKey().equals(key)) {
                    value = p.getContent().trim();
                }
            }
            if (value != null) {
                return value;
            }
        }
        if (parent != null) {
            return parent.getEntry(key);
        }
        if (getParentTestResult() != null) {
            return getParentTestResult().getEntries().get(key);
        }
        return null;
    }

    /**
     * @return the parent
     */
    public ExtEntry getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(final ExtEntry parent) {
        this.parent = parent;
    }

    /**
     * @return the parent of the TetsResult
     */
    public Result getParentTestResult() {
        return parentTestResult;
    }

    /**
     * Set the parent of the test result
     *
     * @param testResult the parent test result
     */
    public void setParentTestResult(final Result testResult) {
        this.parentTestResult = testResult;
    }
}
