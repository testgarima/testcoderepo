package utils.bo;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

//import org.apache.commons.lang3.StringUtils;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "extParam", propOrder = {"param", "extParam"})
public class ExtParam {

    /**
     *
     */
    @XmlElement
    protected List<Param> param;

    /**
     *
     */
    @XmlElement
    protected List<ExtParam> extParam;

    /**
     *
     */
    @XmlAttribute(name = "type", required = true)
    protected String type;

    /**
     *
     */
    @XmlAttribute
    protected String name;

    /**
     * the staging level (DEV,STG,LIVE,...)
     */
    @XmlAttribute
    private String environment;

    /**
     * this property stores the current requestet environement (DEV, STG, LIVE,....)
     */
    @XmlTransient
    private String runningEnv;

    /**
     *
     */
    @XmlTransient
    private ExtParam parent;

    /**
     *
     */
    @XmlTransient
    private TestBase parentTestBase;

    /**
     * Gets the value of the param property.
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the param property.
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getParam().add(newItem);
     * </pre>
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Param }
     *
     * @return List of Param
     */
    public List<Param> getParam() {
        if (param == null) {
            param = new ArrayList<Param>();
        }
        return param;
    }

//    /**
//     * Converts a comma separated string into a List of Long's
//     *
//     * @param key parameter key
//     * @return List with ids
//     */
//    public List<Long> getParamAsListOfLongs(final String key) {
//        final List<Long> longIds = new ArrayList<Long>();
//        final String[] ids = org.springframework.util.StringUtils.commaDelimitedListToStringArray(org.springframework.util.StringUtils
//                .trimAllWhitespace(getParam(key)));
//        if (ids != null) {
//            for (final String s : ids) {
//                longIds.add(Long.valueOf(s));
//            }
//        }
//        return longIds;
//    }
//    /**
//     * Converts a comma separated string into a List of Int's
//     *
//     * @param key parameter key
//     * @return List with ids
//     */
//    public List<Integer> getParamAsListOfInts(final String key) {
//        final List<Integer> intIds = new ArrayList<Integer>();
//        final String[] ids = org.springframework.util.StringUtils.commaDelimitedListToStringArray(org.springframework.util.StringUtils
//                .trimAllWhitespace(getParam(key)));
//        if (ids != null) {
//            for (final String s : ids) {
//                intIds.add(Integer.valueOf(s));
//            }
//        }
//        return intIds;
//    }
//    /**
//     * Converts a comma separated string into a List of String's
//     *
//     * @param key parameter key
//     * @return List with ids
//     */
//    public List<String> getParamAsListOfStrings(final String key) {
//        final List<String> strings = new ArrayList<String>();
//        final String[] ids = org.springframework.util.StringUtils.commaDelimitedListToStringArray(getParam(key));
//        if (ids != null) {
//            for (final String s : ids) {
//                strings.add(s);
//            }
//        }
//        return strings;
//    }
    /**
     * Gets the value of the extParam property.
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the extParam property.
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getExtParam().add(newItem);
     * </pre>
     * <p>
     * Objects of the following type(s) are allowed in the list {@link ExtParam }
     *
     * @return List Of ExtParam
     */
    public List<ExtParam> getExtParam() {
        if (extParam == null) {
            extParam = new ArrayList<ExtParam>();
        }
        return this.extParam;
    }

    /**
     * returns a list of extParam depending on environment and given type
     *
     * @param type type of ExtParam
     * @return list of ExtParam
     */
    public List<ExtParam> getExtParams(final String type) {

        final List<ExtParam> extParams = getExtParam().stream()
                .filter(e -> e.getEnvironment().equals(getRunningEnv()) || e.getEnvironment().equals(""))
                .filter(e -> e.getType().equals(type))
                // .peek(e -> e.setRunningEnv(getRunningEnv()))
                // .peek(e -> e.setParent(this))
                .collect(Collectors.toList());
        extParams.forEach(l -> {
            l.setRunningEnv(runningEnv);
            l.setParent(this);
        });
        return extParams;

    }

    /**
     * returns a list of ext-param-container depending on environment and given type
     *
     * @param type the Type
     * @param name
     * @return List Of ExtParam
     */
    public ExtParam getExtParam(final String type, final String name) {
        return extParam.stream()
                .filter(e -> e.getEnvironment().equals(getRunningEnv()) || e.getEnvironment().equals(""))
                .filter(e -> e.type.equals(type))
                .filter(e -> e.name.equals(name))
                .sorted((e1, e2) -> e2.getEnvironment().compareTo(e1.getEnvironment()))
                .findFirst().get()
                .setRunningEnv(getRunningEnv())
                .setParent(this);
    }

    /**
     * @param key the key
     * @return the integer value
     */
    public int getIntParam(final String key) {
        return Integer.valueOf(getParam(key));
    }

    /**
     * @param key the key
     * @return the big integer value
     */
    public BigInteger getBigIntParam(final String key) {
        return BigInteger.valueOf(getLongParam(key));
    }

    /**
     * @param key the key
     * @return the long value
     */
    public long getLongParam(final String key) {
        return Long.valueOf(getParam(key));
    }

    /**
     * @param key the key
     * @return the double value
     */
    public double getDoubleParam(final String key) {
        return Double.valueOf(getParam(key));
    }

    /**
     * @param key the key
     * @return the boolean value
     */
    public boolean getBooleanParam(final String key) {
        return Boolean.valueOf(getParam(key));
    }

//    /**
//     * see {@link TestCase#getParam(String)}
//     *
//     * @param key the key
//     * @return the big decimal value
//     */
//    public BigDecimal getBigDecimalParam(final String key) {
//        final String value = getParam(key);
//        if (StringUtils.isBlank(value)) {
//            return null;
//        } else {
//            return new BigDecimal(value);
//        }
//    }
    /**
     * Get the value of the type-property
     *
     * @return possible object is {@link String }
     */
    public String getType() {
        return type;
    }

    /**
     * Set the value of the type-property
     *
     * @param value allowed object is {@link String }
     */
    public void setType(final String value) {
        this.type = value;
    }

    /**
     * Get the value of the name-property
     *
     * @return possible object is {@link String }
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of the name-property
     *
     * @param value allowed object is {@link String }
     */
    public void setName(final String value) {
        this.name = value;
    }

    /**
     * @return the environment
     */
    public String getEnvironment() {
        return (environment == null) ? "" : environment;
    }

    /**
     * @param environment the environment to set
     */
    public void setEnvironment(final String environment) {
        this.environment = environment;
    }

    /**
     * @return the environment
     */
    public String getRunningEnv() {
        return runningEnv;
    }

    /**
     * @param staging environment to set
     * @return
     */
    public ExtParam setRunningEnv(final String staging) {
        this.runningEnv = staging;
        return this;
    }

    /**
     * Returns the parameter assigned to the extparam. If the parameter is not configured for the test case the
     * parameter will be read from the test scenario. If you have specified an environment property
     *
     * @param key parameter key
     * @return Parameter or <code>null</code> if no parameter is assigned to the key.
     */
    public String getParam(final String key) {
        if (param != null) {
            final Param get = param.stream()
                    .filter(p -> p.getEnvironment().equals(getRunningEnv()) || p.getEnvironment().equals(""))
                    .filter(p -> p.getKey().equals(key))
                    .sorted((p1, p2) -> p2.getEnvironment().compareTo(p1.getEnvironment()))
                    .findFirst().orElse(null);
            if (get != null) {
                return get.getValue();
            }
        }
        if (parent != null) {
            return parent.getParam(key);
        }
        if (getParentTestBase() != null) {
            return getParentTestBase().getParam(key);
        }

        return null;
    }

    /**
     * @return the parent
     */
    public ExtParam getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     * @return
     */
    public ExtParam setParent(final ExtParam parent) {
        this.parent = parent;
        return this;
    }

    /**
     * @return the parentTestBase
     */
    public TestBase getParentTestBase() {
        return parentTestBase;
    }

    /**
     * @param parentTestBase the parentTestBase to set
     */
    public void setParentTestBase(final TestBase parentTestBase) {
        this.parentTestBase = parentTestBase;
    }
}
