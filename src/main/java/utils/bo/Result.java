package utils.bo;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utils.main.common;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "result", propOrder = {"entry", "extEntry"})
@XmlRootElement(name = "expectedResults")
public class Result {

    /**
     *
     */
    protected static Logger log = LoggerFactory.getLogger(common.class);

    /**
     *
     */
    @XmlElement
    protected List<Entry> entry = new ArrayList<>();

    /**
     *
     */
    @XmlElement
    protected List<ExtEntry> extEntry = new ArrayList<>();

    /**
     *
     */
    @XmlAttribute
    protected String name;

    /**
     *
     */
    @XmlAttribute(name = "type", required = true)
    private String type;

    /**
     *
     */
    @XmlAttribute
    private String environment = "";

    /**
     * this property stores the current requestet environement (DEV, STG, LIVE,....)
     */
    @XmlTransient
    private String runningEnv;

    /**
     * Gets the value of the name property.
     *
     * @return possible object is {@link String }
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value allowed object is {@link String }
     */
    public void setName(final String value) {
        this.name = value;
    }

    /**
     * returns a reference of the entries list
     * 
     * @return list of entries
     */
    public List<Entry> getEntriesList() {
        return entry;
    }

    /**
     * Returns a copy of the entries as Map.
     *
     * @return TestResult entries as Map.
     */
    public Map<String, String> getEntries() {
        final Map<String, String> entries = new HashMap<>();
        for (final Entry e : entry) {
            String v = e.getContent().trim();
            v = v.replaceAll("\\n", "");
            v = v.replaceAll("\\t", "");
            entries.put(e.getKey(), v);
        }
        return entries;
    }

    /**
     * Returns the entries as Map.
     *
     * @param staging - the staging environment
     * @param allowedStagings - all allowed stagings
     * @return TestResult entries as Map with staging.
     */
    public Map<String, String> getEntries(final String staging, final String allowedStagings) {
        final Map<String, String> entries = new HashMap<>();
        final List<String> movedEntries = new ArrayList<>();

        for (final Entry e : entry) {
            String v = e.getContent().trim();
            v = v.replaceAll("\\n", "");
            v = v.replaceAll("\\t", "");
            // System.out.println("e.getKey(): " + e.getKey());
            if (e.getKey().startsWith(staging)) {
                final String newKey = e.getKey().replaceFirst(staging, "");
                entries.put(newKey, v);
                movedEntries.add(newKey);
            } else {
                if (!movedEntries.contains(e.getKey())) {
                    entries.put(e.getKey(), v);
                }
            }
        }

        return entries;
    }

    /**
     * Returns the Result for the given name or null.
     *
     * @param type
     * @param name
     * @return
     */
    public ExtEntry getExtEntry(final String type, final String name) {
        if (extEntry.isEmpty()) {
            return null;
        }
        for (final ExtEntry r : extEntry) {
            if (r.getType().equals(type) && r.getName().equals(name)) {
                r.setRunningEnv(runningEnv);
                return r;
            }
        }
        return null;
    }

    /**
     * Returns the values for the specified key as list of string. The parameter delim is used to separate the string.
     *
     * @param key Key of the entry in the map.
     * @param delim Delimeter
     * @return list containing the separated values as string
     */
    public List<String> getValuesAsString(final String key, final String delim) {
        return getValuesAsString(key, delim, "", "");
    }

    /**
     * Returns the values for the specified key as list of string. The parameter delim is used to separate the string.
     *
     * @param key Key of the entry in the map.
     * @param delim Delimeter
     * @param staging Staging
     * @param allowedStagings
     * @return list containing the separated values as string
     */
    public List<String> getValuesAsString(final String key, final String delim, final String staging, final String allowedStagings) {
        final List<String> values = new ArrayList<String>();
        final String s = "".equals(staging) ? getEntries().get(key) : getEntries(staging, allowedStagings).get(key);
        if (s != null) {
            final StringTokenizer st = new StringTokenizer(s, delim);
            while (st.hasMoreElements()) {
                final String l = (String) st.nextElement();
                values.add(l.trim());
            }
        }

        return values;
    }

    /**
     * Returns a Set view of the keys contained in the TestResult
     *
     * @return a Set view of the keys contained in the TestResult
     */
    public Set<String> keySet() {
        return this.getEntries().keySet();
    }

    /**
     * Returns a Set view of the keys contained in the TestResult
     *
     * @param staging - the staging environment
     * @param allowedStagings - all allowed stagings
     * @return a Set view of the keys contained in the TestResult
     */
    public Set<String> keySet(final String staging, final String allowedStagings) {
        return this.getEntries(staging, allowedStagings).keySet();
    }

    /**
     * Gets the value of the extEntry property.
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the extEntry property.
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getExtEntry().add(newItem);
     * </pre>
     * <p>
     * Objects of the following type(s) are allowed in the list {@link ExtEntry }
     *
     * @return List of ext entry types
     */
    public List<ExtEntry> getExtEntry() {
        return (extEntry != null) ? extEntry : new ArrayList<>();
    }

    /**
     * returns a list of ext-entry-container depending on staging and given type
     *
     * @param type the type
     * @return List of ext entry types
     */
    public List<ExtEntry> getExtEntries(final String type) {
        return extEntry.stream()
                .filter(e -> e.getEnvironment().equals(getRunningEnv()) || e.getEnvironment().equals(""))
                .filter(e -> e.type.equals(type))
                .sorted((e1, e2) -> e2.getEnvironment().compareTo(e1.getEnvironment()))
                .peek(e -> e.setRunningEnv(getRunningEnv()))
                .peek(e -> e.setParentTestResult(this))
                .collect(Collectors.toList());
    }
    
    /**
     *
     * @param type
     * @return
     */
    public ExtEntry getExtEntry(final String type) {
        final List<ExtEntry> extEntries = getExtEntries(type);
        if (!extEntries.isEmpty()){
            return extEntries.get(0);
        }
        return null;
    }

    /**
     * returns a list of ext-entry-container depending on staging and given type
     *
     * @param type the type
     * @param name
     * @return List of ext entry types
     */
    public ExtEntry getExtEntries(final String type, final String name) {
        return extEntry.stream()
                .filter(e -> e.getEnvironment().equals(getRunningEnv()) || e.getEnvironment().equals(""))
                .filter(e -> e.type.equals(type))
                .filter(e -> e.name.equals(name))
                .sorted((e1, e2) -> e2.getEnvironment().compareTo(e1.getEnvironment()))
                .peek(e -> e.setRunningEnv(getRunningEnv()))
                .peek(e -> e.setParentTestResult(this))
                .findFirst().get();
    }

    /**
     * Returns entry for given key as BigDecimal.
     *
     * @param key - the key of the entry
     * @return BigDecimal value of entry
     */
    public BigDecimal getBigDecimalEntry(final String key) {
        return new BigDecimal(getEntries().get(key));
    }

    /**
     * Returns entry for given key as BigDecimal.
     *
     * @param key - the key of the entry
     * @param staging - the staging environment
     * @param allowedStagings - all allowed stagings
     * @return BigDecimal value of entry
     */
    public BigDecimal getBigDecimalEntry(final String key, final String staging, final String allowedStagings) {
        return new BigDecimal(getEntries(staging, allowedStagings).get(key));
    }

    /**
     * Returns entry for given key as BigInteger.
     *
     * @param key - the key of the entry
     * @return BigInteger value of entry
     */
    public BigInteger getBigIntEntry(final String key) {
        return BigInteger.valueOf(getLongEntry(key));
    }

    /**
     * Returns entry for given key as BigInteger.
     *
     * @param key - the key of the entry
     * @param staging - the staging environment
     * @param allowedStagings - all allowed stagings
     * @return BigInteger value of entry
     */
    public BigInteger getBigIntEntry(final String key, final String staging, final String allowedStagings) {
        return BigInteger.valueOf(getLongEntry(key, staging, allowedStagings));
    }

    /**
     * Returns entry for given key as boolean.
     *
     * @param key - the key of the entry
     * @return Boolean value of entry
     */
    public boolean getBooleanEntry(final String key) {
        return Boolean.valueOf(getEntries().get(key));
    }

    /**
     * Returns entry for given key as boolean.
     *
     * @param key - the key of the entry
     * @param staging - the staging environment
     * @param allowedStagings - all allowed stagings
     * @return Boolean value of entry
     */
    public boolean getBooleanEntry(final String key, final String staging, final String allowedStagings) {
        return Boolean.valueOf(getEntries(staging, allowedStagings).get(key));
    }

    /**
     * Returns entry for given key as Byte.
     *
     * @param key - the key of the entry
     * @return Byte value of entry
     */
    public Byte getByteEntry(final String key) {
        return Byte.valueOf(getEntries().get(key));
    }

    /**
     * Returns entry for given key as Byte.
     *
     * @param key - the key of the entry
     * @param staging - the staging environment
     * @param allowedStagings - all allowed stagings
     * @return Byte value of entry
     */
    public Byte getByteEntry(final String key, final String staging, final String allowedStagings) {
        return Byte.valueOf(getEntries(staging, allowedStagings).get(key));
    }

    /**
     * Returns entry for given key as double.
     *
     * @param key - the key of the entry
     * @return Double value of entry
     */
    public double getDoubleEntry(final String key) {
        return Double.valueOf(getEntries().get(key));
    }

    /**
     * Returns entry for given key as double.
     *
     * @param key - the key of the entry
     * @param staging - the staging environment
     * @param allowedStagings - all allowed stagings
     * @return Double value of entry
     */
    public double getDoubleEntry(final String key, final String staging, final String allowedStagings) {
        return Double.valueOf(getEntries(staging, allowedStagings).get(key));
    }

    /**
     * Returns entry for given key as int.
     *
     * @param key - the key of the entry
     * @return Integer value of entry
     */
    public int getIntEntry(final String key) {
        return Integer.valueOf(getEntries().get(key));
    }

    /**
     * Returns entry for given key as int.
     *
     * @param key - the key of the entry
     * @param staging - the staging environment
     * @param allowedStagings - all allowed stagings
     * @return Integer value of entry
     */
    public int getIntEntry(final String key, final String staging, final String allowedStagings) {
        return Integer.valueOf(getEntries(staging, allowedStagings).get(key));
    }

    /**
     * Returns entry for given key as long.
     *
     * @param key - the key of the entry
     * @return Long value of entry
     */
    public long getLongEntry(final String key) {
        return Long.valueOf(getEntries().get(key));
    }

    /**
     * Returns entry for given key as long.
     *
     * @param key - the key of the entry
     * @param staging - the staging environment
     * @param allowedStagings - all allowed stagings
     * @return Long value of entry
     */
    public long getLongEntry(final String key, final String staging, final String allowedStagings) {
        return Long.valueOf(getEntries(staging, allowedStagings).get(key));
    }

    /**
     * Returns entry for given key as Short.
     *
     * @param key - the key of the entry
     * @return Short value of entry
     */
    public Short getShortEntry(final String key) {
        return Short.valueOf(getEntries().get(key));
    }

    /**
     * Returns entry for given key as short.
     *
     * @param key - the key of the entry
     * @param staging - the staging environment
     * @param allowedStagings - all allowed stagings
     * @return Short value of entry
     */
    public Short getShortEntry(final String key, final String staging, final String allowedStagings) {
        return Short.valueOf(getEntries(staging, allowedStagings).get(key));
    }

    /**
     * Returns entry for given key as String.
     *
     * @param key - the key of the entry
     * @return String value of entry
     */
    public String getStringEntry(final String key) {
        return getEntries().get(key);
    }

    /**
     * Returns entry for given key as String.
     *
     * @param key - the key of the entry
     * @param staging - the staging environment
     * @param allowedStagings - all allowed stagings
     * @return String value of entry
     */
    public String getStringEntry(final String key, final String staging, final String allowedStagings) {
        return getEntries(staging, allowedStagings).get(key);
    }

    /**
     * @return the type or an empty string if not set
     */
    public String getType() {
        if (type != null) {
            return type;
        } else {
            return "";
        }
    }

    /**
     *
     * @param type
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * @return the environment
     */
    public String getEnvironment() {
        return environment;
    }

    /**
     * @param environment the environment to set
     */
    public void setEnvironment(final String environment) {
        this.environment = environment;
    }

    /**
     * @return the runningEnv
     */
    public String getRunningEnv() {
        return runningEnv;
    }

    /**
     * @param runningEnv the runningEnv to set
     */
    public void setRunningEnv(final String runningEnv) {
        this.runningEnv = runningEnv;
    }

}
