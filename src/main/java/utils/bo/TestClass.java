package utils.bo;


import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.arvato-mobile.de/qa}test-base">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.arvato-mobile.de/qa}test-scenario" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="class" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "testScenario" })
@XmlRootElement(name = "testClass")
public class TestClass extends TestBase {

    /**
     *
     */
    @XmlElement(name = "testScenario")
    protected List<TestScenario> testScenario;

    /**
     *
     */
    @XmlAttribute(name = "id", required = true)
    protected String name;

    /**
     *
     */
    @XmlAttribute(name = "class", required = true)
    protected String clazz;

    /**
     * Gets the value of the testScenario property.
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the testScenario property.
     * <p>
     * For example, to add a new item, do as follows:
     *
     * <pre>
     * getTestScenario().add(newItem);
     * </pre>
     * <p>
     * Objects of the following type(s) are allowed in the list {@link TestScenario }
     * @return
     */
    public List<TestScenario> getTestScenario() {
        if (testScenario == null) {
            testScenario = new ArrayList<>();
        }
        return this.testScenario;
    }

    /**
     * Get the TestScenario that belongs to a method
     *
     * @param methodName name of the method
     * @return the testscenario
     */
    public TestScenario getTestScenario(final String methodName) {
        final List<TestScenario> scenarios = getTestScenario();
        TestScenario scenario = null;
        for (final TestScenario ts : scenarios) {
            if (ts.getMethod().equals(methodName)) {
                scenario = ts;
                break;
            }
        }
        return scenario;
    }

    /**
     * Gets the value of the name property.
     *
     * @return possible object is {@link String }
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value allowed object is {@link String }
     */
    @Override
    public void setName(final String value) {
        this.name = value;
    }

    /**
     * Gets the value of the clazz property.
     *
     * @return possible object is {@link String }
     */
    public String getClazz() {
        return clazz;
    }

    /**
     * Sets the value of the clazz property.
     *
     * @param value allowed object is {@link String }
     */
    public void setClazz(final String value) {
        this.clazz = value;
    }

}
