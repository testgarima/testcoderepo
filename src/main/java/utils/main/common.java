package utils.main;

import java.lang.reflect.Method;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.TestException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.bo.ExtParam;
import utils.bo.Param;

import utils.bo.TestCase;
import utils.bo.TestClass;
import utils.bo.TestData;
import utils.bo.TestScenario;
import utils.tools.QaComparator;


/**
 * common  class for the Selenium and API Tests. It reads and provides the test data using xmls and provides methods to compare values from qa comparator.
 *
 *
 */
public abstract class common extends QaComparator {

    /**
     * A divider for logs consisting of plus signs.
     */
    private static final String LOG_DIVIDER_PLUS = "++++++++++++++++++++++++++++++++++++++++++++++++++";

    /**
     * A divider for logs consisting of minus signs.
     */
    private static final String LOG_DIVIDER_MINUS = "--------------------------------------------------";

    /**
     * A divider for logs consisting of minus signs.
     */
    private static final String LOG_DIVIDER_STAR = "**************************************************";

    
    /**
     *
     */
    private static final String LOG_LF = "\n";

   

    /**
     *
     */
    private static final String LOG_PLUS = "+ ";

   
    /**
     *
     */
    private static final long DISPLAY_DELAY = 10L;

    /**
     *
     */
    protected TestData testData;

    /**
     *
     */
    protected Logger log = LoggerFactory.getLogger(this.getClass());

    

    

   
    /**
     *
     */
    protected static final String GLOBAL_STORE = "GlobalStore";

    

    /**
     * Initializes the test. It calls setUp(String).
     * @throws java.lang.Exception
     */
    protected void setUp() throws Exception {
        setUp("");
    }

    /**
     * Reads the test data from a file.
     *
     * @param testDataFile Name of the xml file. When an empty string is passed, the name is build using the
     *            instantiating class + .xml.
     */
    
    @Parameters({ "testDataFile" })
    protected void setUp(@Optional("") String testDataFile) {

        final String clazzName = this.getClass().getSimpleName();

        try {
            log.info("common::setUp()");
            if (testDataFile == null || testDataFile.length() < 1) {
                testDataFile = clazzName + ".xml";
            }
            testData = TestData.getInstance(testDataFile);
            testData.setTestClassName(this.getClass().getName());
            

            logPlusDivider();
            log.info("+ Running the test " + testDataFile + " against: " +""); //stagingEnv);
            logPlusDivider();



        } catch (final Exception ex) {
            java.util.logging.Logger.getLogger(common.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            errorSummary.add(null, "could not read test data: " + testDataFile);
            Assert.fail("could not read test data: " + testDataFile);
        }
    }

    /**
     * displays the ErrorSummary if not empty
     *
     * @throws Exception
     */
    @AfterClass
    protected void tearDown() throws Exception {
        

        if (!errorSummary.isEmpty()) {
            displayErrorSummary(errorSummary.getSummary());
        }
    }

    /**
     * Enum to define status color for TestScenarios
     */
    public enum StatusColor {

        /**
         * all works fine
         */
        green,
        /**
         * there are skipped tests. Reason could be: known issues or depended tests are failing
         */
        yellow,
        /**
         * one ore more tests are failing
         */
        red

    }

    /**
     * Runs the given {@link TestScript} for every {@link TestCase} of the current {@link TestScenario}.
     *
     * @param testScript the test script to be executed
     */
    public void executeForEveryTestCase(final TestScript testScript) {
        final TestScenario testScenario = getTestScenario();
        final String testScenarioName = testScenario.getName();
        StatusColor testScenarioStatus = StatusColor.green;
        logAsInfoHeadlineStars("Starting to run TestScenario: " + testScenarioName);
        for (final TestCase testCase : testScenario.getTestCases()) {

                testCase.setScenarioName(testScenarioName);


                final long startTimestamp = System.currentTimeMillis();
                Long endTimestamp = null;

                

                final StringBuilder resultCommentString = new StringBuilder();
                resultCommentString.append("TestCase " + testCase.getName() + ": ");

                    try {
                        testScript.execute(testCase);
                        endTimestamp = System.currentTimeMillis();
                        
                    } catch (final Throwable e) {

                    }

                    if (endTimestamp == null) {
                        endTimestamp = System.currentTimeMillis();
                    }

                    if (hasErrors(testCase)) {
                    	testScenarioStatus = StatusColor.red;
                    	resultCommentString.append("FAILED");
                        resultCommentString.append("\n" + errorSummary.getMessagesForTestCase(testCase));
                    } else {
                      
                    	testScenarioStatus = StatusColor.green;
                    	resultCommentString.append("Passed");
                    }
                } 
                

                
        
        logAsInfoHeadline("Finished to run TestScenario: " + testScenarioName);

        final String testScenarioErrorSummary = errorSummary.getSummary(testScenarioName);
        if (StringUtils.isNotBlank(testScenarioErrorSummary)) {
            displayErrorSummary(testScenarioErrorSummary);
        }

        switch (testScenarioStatus) {
            case green:
                // all works fine
                break;
            case yellow:
                // there is a known issue, so set the test status to yellow
                throw new SkipException(getErrorSummaryForTestScenario(testScenario));
            case red:
                // this test does not work as expected
                Assert.fail(getErrorSummaryForTestScenario(testScenario));
        }

    }

    

   
    
    

   

  

  
   
    /**
     * Checks if any errors have been found for the given test case.
     *
     * @param testCase the test case that should be checked
     * @return true, if errors have been reported for that test case
     */
    protected boolean hasErrors(final TestCase testCase) {
        return errorSummary.hasErrors(testCase);
    }

    /**
     * Get the current number of errors for a testCase
     *
     * @param testCase the test case that should be checked
     * @return the number of errors
     */
    protected int getCurrentNumberOfTestcaseErrors(final TestCase testCase) {
        return errorSummary.getCurrentNumberOfTestcaseErrors(testCase);
    }

    
    
    
    
    
    
    
    
    
   
    /**
     * Returns the TestScenario assigned to the method name of the calling method. When no Test Scenario is found, an
     * assertion failure is called.
     *
     * @return TestScenario the TestScenario instance
     */
    protected TestScenario getTestScenario() {
        final StackTraceElement element = getTestMethodStackElement();
        TestScenario ts = testData.getTestScenario(element.getMethodName());
        if (ts == null && testData.getTestClass(element.getFileName()) != null) {
            ts = testData.getTestClass(element.getFileName()).getTestScenario(element.getMethodName());
        }
        if (ts == null) {
            writeToErrorSummary(null, "No TestScenario assigned the method: " + element.getMethodName());
            Assert.fail("No TestScenario assigned to the method: " + element.getMethodName());
        }
        log.info("Scenario: " + ts.getName());
        return ts;
    }

    /**
     * Returns the TestClass assigned to the method name of the calling method. When no Test Class is found, an empty
     * TestClass instance is returned.
     *
     * @return TestClass
     */
    protected TestClass getTestClass() {
        final StackTraceElement element = getTestMethodStackElement();
        TestClass tc = testData.getTestClass(element.getFileName());
        if (tc == null) {
            log.warn("No TestClass assigned to method: " + element.getMethodName());
            tc = new TestClass();
        } else {
            log.info("TestClass: " + tc.getName());
        }
        tc.setTestClassName(element.getFileName());
        return tc;
    }

    /**
     * Gets the currently executed test method. Loops through the stack trace looking for the parameterless method
     * annotated with <code>org.testng.annotations.Test</code>. Throws the <code>org.testng.TestException</code> when no
     * such a method is present in current stack trace.
     *
     * @see org.testng.annotations.Test
     * @return the stack trace element representing the test method execution point.
     */
    protected StackTraceElement getTestMethodStackElement() {
        final Throwable t = new Throwable();
        final StackTraceElement[] elements = t.getStackTrace();
        Method method = null;
        for (final StackTraceElement element : elements) {
            // following trial will succeed only for parameterless methods
            try {
                method = Class.forName(element.getClassName()).getMethod(element.getMethodName());
            } catch (final Throwable e) {
                // will fail for a method with parameters, which cannot be the test method case.
                continue;
            }
            if (method != null && method.isAnnotationPresent(Test.class)) {
                return element;
            }
        }
        writeToErrorSummary(null, "The 'getTestMethodStackElement()' method called outside of any test method scope.");
        throw new TestException("The 'getTestMethodStackElement()' method called outside of any test method scope.");
    }

   
    
    /**
     * Writes an entry into the errorSummary Map
     *
     * @param testCase
     * @param errorMessage
     */
    @Override
    protected void writeToErrorSummary(final TestCase testCase, final String errorMessage) {
        log.error(errorMessage);
        errorSummary.add(testCase, errorMessage);
    }

    /**
     * Sleep for n millisecs.
     * <p/>
     * Handle sleeping in tests with care! Long running tests lead to late feedback, people might not wait until tests
     * are finished or start skipping tests completely when they take too long.
     *
     * @param time the sleepingtime in millisecs
     */
    protected void sleep(final long time) {
        if (time == 0) {
            return;
        }

        try {
            log.info("Sleeping for " + time + "ms.");
            Thread.sleep(time);
        } catch (final InterruptedException exc) {
            // ignore this
        }
    }

    /**
     * get the error summary for the testscenario
     *
     * @param testScenario
     * @return
     */
    protected String getErrorSummaryForTestScenario(final TestScenario testScenario) {
        return errorSummary.getSummary(testScenario.getName());
    }

    /**
     * Displays the error summary
     * @param errorSummaryString
     */
    protected void displayErrorSummary(final String errorSummaryString) {
        // wait a little, so that the summary will be written after the last log
        sleep(DISPLAY_DELAY);
        System.err.println(LOG_LF + "*************************************************************************");
        System.err.println("ERROR SUMMARY");
        System.err.println(errorSummaryString);
        System.err.println(LOG_LF + "*************************************************************************" + LOG_LF);
    }

    /**
     * Logs a row of plus signs, for prettier logs.
     */
    protected void logPlusDivider() {
        log.info(LOG_DIVIDER_PLUS);
    }

    /**
     * Logs the given message as a headline, surrounded by lines of star dividers.
     * <p/>
     * The log level will be {@link Level#INFO}.
     *
     * @param message the message to be logged
     */
    protected void logAsInfoHeadlineStars(final String message) {
        log.info(LOG_DIVIDER_STAR);
        log.info(LOG_PLUS + message);
        log.info(LOG_DIVIDER_STAR + LOG_LF);
    }

    /**
     * Logs the given message as a headline, surrounded by lines of dividers.
     * <p/>
     * The log level will be {@link Level#INFO}.
     *
     * @param message the message to be logged
     */
    protected void logAsInfoHeadline(final String message) {
        log.info(LOG_DIVIDER_PLUS);
        log.info(LOG_PLUS + message);
        log.info(LOG_DIVIDER_PLUS + LOG_LF);
    }

    /**
     * Logs the given message as a headline, surrounded by lines of dividers.
     * <p/>
     * The log level will be {@link Level#INFO}.
     *
     * @param message the message to be logged
     */
    protected void logAsErrorHeadline(final String message) {
        log.error(LOG_DIVIDER_PLUS);
        log.error(LOG_PLUS + message);
        log.error(LOG_DIVIDER_PLUS + LOG_LF);
    }

    /**
     * Logs a row of minus signs, for prettier logs.
     */
    protected void logMinusDivider() {
        log.info(LOG_DIVIDER_MINUS);
    }

    /**
     * Logs a row of star signs, for prettier logs.
     */
    protected void logStarDivider() {
        log.info(LOG_DIVIDER_STAR);
    }

    /**
     * Gets the error summary.
     *
     * @return the error summary
     */
    public ErrorSummary getErrorSummary() {
        return errorSummary;
    }

    
    /**
     *
     * @return
     */
    public ExtParam getTestDataStore() {
        ExtParam store;
        if (testData.getExtParams(GLOBAL_STORE).isEmpty()) {
            store = new ExtParam();
            store.setType(GLOBAL_STORE);
            testData.getExtParam().add(store);
        } else {
            store = testData.getExtParams(GLOBAL_STORE).get(0);
        }
        return store;
    }

    /**
     * adds or replace the given values to the global store
     *
     * @param key
     * @param value
     */
    public void addToTestDataStore(final String key, final String value) {
        final Param param = new Param();
        param.setKey(key);
        param.setValue(value);
        log.info("Adding testData-Store: " + key + " -> " + value);
        final ExtParam store = getTestDataStore();
        final List<Param> globals = store.getParam();

        for (final Param global : globals) {
            if (global.getKey().equals(param.getKey())) {
                globals.remove(global);
                break;
            }
        }
        globals.add(param);
    }

    /**
     * gets a param from API_GLOBAL_STORE
     *
     * @param key
     * @return
     */
    public String getParamFromGlobalStore(final String key) {

        String value = null;
        if (!testData.getExtParams(GLOBAL_STORE).isEmpty()) {
            final ExtParam store = testData.getExtParams(GLOBAL_STORE).get(0);

            for (final Param param : store.getParam()) {
                if (key.equals(param.getKey())) {
                    value = param.getValue();
                    break;
                }
            }
        }
        return value;
    }

    /**
     * iterates over variables and substitutes in the given value all occurrences. The variables should be between
     * double percent sign like "Hello %%how%%". The '%%how%%' will then replaced by the value of global store.
     *
     * @see addToTestDataStore
     * @param value
     * @return substituted value
     */
    public String substitute(String value) {

        if (value == null) {
            return value;
        }

        if (!testData.getExtParams(GLOBAL_STORE).isEmpty()) {
            final ExtParam store = testData.getExtParams(GLOBAL_STORE).get(0);
            for (final Param param : store.getParam()) {
                if (param.getKey() != null && param.getValue() != null) {
                    value = value.replaceAll("%%" + param.getKey() + "%%", param.getValue());
                }
            }
        }
        return value;
    }
}
