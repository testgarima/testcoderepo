package utils.main;

import utils.bo.TestCase;

/**
 * The interface for every executable test script.
 */
public interface TestScript {

    /**
     * The test script that should be executed for the testCase.
     *  @param testCase the test case the script should be executed for
     * @throws Exception the exception
     */
    void execute(TestCase testCase) throws Exception;
}
