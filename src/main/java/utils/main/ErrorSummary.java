package utils.main;

import utils.bo.TestCase;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This class holds all error messages to display them on the end of all tests.
 *
 * 
 */
public class ErrorSummary {

    /**
     * This Map holds the summary for every failed test class Key of Map: testClassName Value of Map: TestClassData
     */
    private static final Map<String, TestClassData> summary = new LinkedHashMap<>();

    /**
     * holds all unknown errors
     */
    private static final ArrayList<String> unkownError = new ArrayList<>();

    /**
     * The instance of ErrorSummary
     */
    private static final ErrorSummary instance = new ErrorSummary();

    /**
     * The instance of ErrorSummary get added a shut down hook to display a summary at the end
     */
    public ErrorSummary() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                printSummary();
            }
        });
    }

    /**
     * returns the instance of ErrorSummary
     *
     * @return
     */
    public static ErrorSummary getInstance() {
        return instance;
    }

    /**
     * checks if the there exists some error message entries
     *
     * @return true if empty
     */
    public boolean isEmpty() {
        return summary.isEmpty() && unkownError.isEmpty();
    }

    /**
     * Adds a new summary entry for the given testCase and errorMessage
     *
     * @param testCase
     * @param errorMessage
     */
    public void add(final TestCase testCase, final String errorMessage) {
        if (testCase == null) {
            unkownError.add(errorMessage);
        } else {
            final String testClassName = testCase.getTestClassName();
            if (summary.containsKey(testClassName)) {
                final TestClassData testClassData = summary.get(testClassName);
                testClassData.add(testCase, errorMessage);
            } else {
                final TestClassData testClassData = new TestClassData(testCase, errorMessage);
                summary.put(testClassName, testClassData);
            }
        }
    }

    /**
     * generates a summary ordered by testScenario, testCase and errorSummary
     *
     * @return formated summary
     */
    public String getSummary() {
        final StringBuilder msg = new StringBuilder();
        for (final String testClassName : summary.keySet()) {
            final StringBuilder hashes = new StringBuilder().append('\n');
            for (int i = 0; i < testClassName.length() + 10; i++) {
                hashes.append('-');

            }
            msg.append('\n').append(hashes);
            msg.append("\n---  ").append(testClassName).append("  ---\n").append(hashes);

            for (final String scenarioId : summary.get(testClassName).getTestScenaios().keySet()) {
                msg.append(getSummary(scenarioId));
            }
        }

        if (!unkownError.isEmpty()) {
            msg.append("\n## UNKNOWN ERROR:");
            for (final String unkwown : unkownError) {
                msg.append("\n\t* ").append(unkwown);
            }
        }
        return msg.toString();
    }

    /**
     * This method prints an over all summary at the end of all tests
     */
    private void printSummary() {
        final StringBuilder msg = new StringBuilder();
        msg.append("\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
        msg.append("\t\tOVER ALL ERROR SUMMARY");
        msg.append("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n");
        if (summary.isEmpty() && unkownError.isEmpty()) {
            msg.append("  ALL WORKS FINE :-)\n");
        } else {
            msg.append(getSummary());
        }
        msg.append("\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
        System.err.println(msg);

    }

    /**
     * generates a summary for the given scenario. It contains all failed testCases of this scenario
     *
     * @param scenarioId
     * @return formated summary for the scenario
     */
    public String getSummary(final String scenarioId) {
        final StringBuilder msg = new StringBuilder();

        for (final String testClassName : summary.keySet()) {
            if (summary.get(testClassName).getTestScenaios().containsKey(scenarioId)) {

                getSummaryForScenario(testClassName, scenarioId, msg);
            }
        }
        return msg.toString();
    }

    /**
     *
     * @param testClassName
     * @param scenarioId
     * @param msg
     */
    private void getSummaryForScenario(final String testClassName, final String scenarioId, final StringBuilder msg) {
        final ScenarioData scenarioData = summary.get(testClassName).getTestScenaios().get(scenarioId);
        if (scenarioData != null) {
            msg.append("\n\n## TestScenario ").append(scenarioId).append(" : ").append(scenarioData.getScenarioComment()).append("\n");

            final Map<String, TestCaseData> testCases = scenarioData.getTestCases();

            for (final Map.Entry<String, TestCaseData> testCase : testCases.entrySet()) {

                final String testCaseId = testCase.getKey();
                final TestCaseData testCaseData = testCase.getValue();
                msg.append("\nTestCase: ").append(testCaseId).append(": ").append(testCaseData.getTestCaseComment()).append("\n");

                for (final String errormsg : testCaseData.getErrorMessages()) {
                    msg.append('\t').append(errormsg).append('\n');
                }
            }
        }
    }

    /**
     * checks if for the given testCase a error entry exists
     *
     * @param testCase
     * @return true if exist
     */
    public boolean hasErrors(final TestCase testCase) {

        if (summary.containsKey(testCase.getTestClassName())) {

            final Map<String, ScenarioData> scenaios = summary.get(testCase.getTestClassName()).getTestScenaios();
            if (scenaios.containsKey(testCase.getParent().getName())) {
                final ScenarioData scenario = scenaios.get(testCase.getParent().getName());
                return scenario.getTestCases().containsKey(testCase.getName());
            }
        }
        return false;
    }


    /**
     * get error messages for testCase
     *
     * @param testCase
     * @return
     */
    public String getMessagesForTestCase(final TestCase testCase) {

        if (summary.containsKey(testCase.getTestClassName())) {

            final Map<String, ScenarioData> scenaios = summary.get(testCase.getTestClassName()).getTestScenaios();
            if (scenaios.containsKey(testCase.getParent().getName())) {
                final ScenarioData scenario = scenaios.get(testCase.getParent().getName());
                final List<String> messageList = scenario.getTestCases().get(testCase.getName()).getErrorMessages();
                final StringBuilder messages = new StringBuilder();
                boolean firstMessage = true;
                for (final String message : messageList) {
                    if (firstMessage) {
                        firstMessage = false;
                        messages.append(message);
                    } else {
                        messages.append("\n" + message);
                    }
                }
                return messages.toString();
            }
        }
        return "";
    }

    /**
     * gets the current number of errors
     *
     * @param testCase
     * @return size of error summary
     */
    public int getCurrentNumberOfTestcaseErrors(final TestCase testCase) {
        int numberOfErrors = 0;
        if (summary.containsKey(testCase.getTestClassName())) {
            final Map<String, ScenarioData> scenaios = summary.get(testCase.getTestClassName()).getTestScenaios();

            if (scenaios.containsKey(testCase.getParent().getName())) {
                final TestCaseData testCaseData = scenaios.get(testCase.getParent().getName()).getTestCases().get(testCase.getName());
                if (testCaseData != null) {
                    numberOfErrors = testCaseData.getErrorMessages().size();
                }
            }
        }
        return numberOfErrors;
    }
}


class TestClassData {

    /**
     *
     */
    private final Map<String, ScenarioData> scenarios = new LinkedHashMap<>();

    /**
     *
     */
    private final String testClassName;

    /**
     *
     * @param testCase
     * @param errorMessage
     */
    public TestClassData(final TestCase testCase, final String errorMessage) {
        testClassName = testCase.getTestClassName();
        final ScenarioData scenarioData = new ScenarioData(testCase, errorMessage);
        scenarios.put(testCase.getParent().getName(), scenarioData);
    }

    /**
     *
     * @param testCase
     * @param errorMessage
     */
    void add(final TestCase testCase, final String errorMessage) {
        final String scenarioId = testCase.getParent().getName();
        if (scenarios.containsKey(scenarioId)) {
            final ScenarioData scenario = scenarios.get(scenarioId);
            scenario.add(testCase, errorMessage);
        } else {
            final ScenarioData scenarioData = new ScenarioData(testCase, errorMessage);
            scenarios.put(scenarioId, scenarioData);
        }
    }

    /**
     *
     * @return the test scenarios which are failed
     */
    Map<String, ScenarioData> getTestScenaios() {
        return scenarios;
    }

    /**
     * @return the testClassName
     */
    public String getTestClassName() {
        return testClassName;
    }

}


class ScenarioData {

    /**
     * holds the comment of a scenario
     */
    private final String scenarioComment;

    /**
     * holds error messages of a TestCase
     */
    private final Map<String, TestCaseData> testCases = new LinkedHashMap<>();

    /**
     *
     * @param testCase
     * @param errorMessage
     */
    ScenarioData(final TestCase testCase, final String errorMessage) {
        if (testCase.getParent().getComment() != null) {
            this.scenarioComment = testCase.getParent().getComment();
        } else {
            this.scenarioComment = "";
        }
        final TestCaseData testCaseData = new TestCaseData(testCase, errorMessage);
        testCases.put(testCase.getName(), testCaseData);
    }

    /**
     * @return the scenarioComment
     */
    String getScenarioComment() {
        return scenarioComment;
    }

    /**
     *
     * @param testCase
     * @param errorMessage
     */
    void add(final TestCase testCase, final String errorMessage) {
        final String testCaseId = testCase.getName();
        if (testCases.containsKey(testCaseId)) {
            final TestCaseData testCaseData = testCases.get(testCaseId);
            testCaseData.add(errorMessage);

        } else {
            final TestCaseData testCaseData = new TestCaseData(testCase, errorMessage);
            testCases.put(testCaseId, testCaseData);
        }
    }

    /**
     *
     * @return
     */
    Map<String, TestCaseData> getTestCases() {
        return testCases;
    }

}


class TestCaseData {

    /**
     *
     */
    private final String testCaseComment;

    /**
     *
     */
    private final List<String> errorMessages;

    /**
     *
     * @param testCase
     * @param errorMessage
     */
    TestCaseData(final TestCase testCase, final String errorMessage) {
        this.testCaseComment = testCase.getComment();
        this.errorMessages = new LinkedList<>();
        this.errorMessages.add(errorMessage);
    }

    /**
     * @return the testCaseComment
     */
    String getTestCaseComment() {
        return testCaseComment;
    }

    /**
     *
     * @param errorMessage
     */
    void add(final String errorMessage) {
        this.errorMessages.add(errorMessage);
    }

    /**
     *
     * @return
     */
    List<String> getErrorMessages() {
        return this.errorMessages;
    }

}
