package apitest;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;
import com.jayway.jsonpath.JsonPath;
import utils.bo.ExtEntry;
import utils.bo.Result;
import utils.bo.TestCase;
import utils.main.TestScript;
import utils.main.common;

/**
 * @author GACH2
 * This class test methods test api calls
 * verfies apicallstatuscode
 * verifies returned json content
 * expected values are picked from testapidata.xml
 *
 */
public class TestApiCall extends common{
	
	@Override
    @BeforeClass(alwaysRun = true)
    @Parameters("testDataFile")
    protected void setUp(@Optional("testapidata.xml") final String testDataFile) {
        super.setUp(testDataFile);
    }
	
	
	@Test
    public void testAPICallStatus() throws Exception {
    	executeForEveryTestCase(new TestScript() {
    		@Override
            public void execute(final TestCase testCase) throws Exception {
            testMimeType(testCase.getParam("path"), testCase.getParam("Accept"));
            testStatusCode(testCase.getParam("path"), testCase.getResults().get(0).getIntEntry("responseCode"));
    			}});
    }
	
	@Test
    public void testAPICallContent() throws Exception {
    	executeForEveryTestCase(new TestScript() {

            @Override
            public void execute(final TestCase testCase) throws Exception {
            	testContentJSON(testCase, testCase.getParam("path"));

            }});
    }

	
	public static void testStatusCode(String restURL, int expectedStatusCode) throws ClientProtocolException, IOException {

		HttpUriRequest request = new HttpGet(restURL);
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

		Assert.assertEquals(httpResponse.getStatusLine().getStatusCode(),expectedStatusCode);
		}

	public static void testMimeType(String restURL, String expectedMimeType) throws ClientProtocolException, IOException {

		HttpUriRequest request = new HttpGet(restURL);
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

		Assert.assertEquals(expectedMimeType,ContentType.getOrDefault(httpResponse.getEntity()).getMimeType());
		}
	
	
	public void testContentJSON(final TestCase testCase,String restURL) throws ClientProtocolException, IOException, SAXException, ParserConfigurationException, JSONException, java.text.ParseException {

		HttpUriRequest request = new HttpGet(restURL);
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		// Convert the response to a String format
		String result = EntityUtils.toString(httpResponse.getEntity());
        System.out.println(httpResponse +"http response"+result);
        compareRawResponse(testCase, result);
        }
	
	
     private void compareRawResponse(final TestCase testCase, final String jsonResponse) throws java.text.ParseException {

	final Result expectedResult = testCase.getResults().get(0);
    final List<ExtEntry> jsonPathes = expectedResult.getExtEntries("JsonPath");

    log.info("Response " + jsonResponse);

    for (final ExtEntry expectedKeyValue : jsonPathes) {
        final String jsonpath = expectedKeyValue.getEntry("jsonPath");
        final String expectedValue = expectedKeyValue.getEntry("value");
        String currentValue = "";

        try {
            currentValue = JsonPath.read(jsonResponse, jsonpath).toString();
        } catch (final ParseException e) {
            writeToErrorSummary(testCase, "JsonPath not found: " + jsonpath);
        } catch (final NullPointerException e) {
            writeToErrorSummary(testCase, "JsonPath not found: " + jsonpath);
        }

        if (currentValue.length() > 0) {
            compareObjectValue(testCase, jsonpath, expectedValue, currentValue);
        } else {
            writeToErrorSummary(testCase, "JsonPath not found: " + jsonpath);
        }
    
    }
}
}






