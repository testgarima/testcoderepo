package selenium.featurepages;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.bo.Result;
import utils.bo.TestCase;
import selenium.base.BaseSeleniumWebPage;

/**
 * @author GACH2
 * This page class has logic for validating elements on Conichi Home page https://www.conichi.com/en/
 * and also has navigation method which helps in navigating to other pages
 *
 */

public class ConichiMainPage extends BaseSeleniumWebPage{

	protected String xpath;
	protected  WebDriverWait _wait;
	protected WebDriver driver;
	private By menuLocator=By.xpath("//div[@id='bgnav']");

	private By cookiesTermsLocator=By.xpath("//p[contains(text(),'We use cookies to personalise content and to analyse our traffic.  We also share information about your use of our site with our analytics partners.  You consent to our cookies if you continue to use this website.')]");

	
	private By phoneImageLocator=By.xpath("//img[@title='screen_explore_app_english_june_2017']");
	
	
	public ConichiMainPage(final WebDriver driver) {
		super(driver);
		this.driver = super.getDriver();
		this._wait = super.get_wait();
	}


	/**
	 * validate the bottom section for cookies storage terms notification
	 *
	 * @param testCase
	 * @throws InterruptedException 
	 * 
	 * 
	 */
	public void validateElements(final TestCase testCase) throws InterruptedException {
		checkBottom(testCase);
		checkPhoneImage(testCase);

	}

	protected void checkBottom(final TestCase testCase) {
		

		final Result expectedResult = testCase.getResultByType("validateBottom");
		final boolean expectedBottomElement = expectedResult.getBooleanEntry("TermsNotification");

		final WebElement cookiesTermsElement = driver.findElement(cookiesTermsLocator);

		log.info("validate bottom notification is displayed ");
		// compare expected and actual
		compareObjectValue(testCase, "Bottom Cookies Notification is being compared ", expectedBottomElement, cookiesTermsElement.isDisplayed());


	}
	
	
	
	
	protected void checkPhoneImage(final TestCase testCase) throws InterruptedException {
		
        log.info("validation phone image on main page start");

		
		final Result expectedResult = testCase.getResultByType("validatePhoneImage");
		

		final boolean expectedElement = expectedResult.getBooleanEntry("phoneimage");
		

		
		final WebElement actual = driver.findElement(phoneImageLocator);
		


		// compare expected and actual
        log.info("validation phone image on main page start");

		compareObjectValue(testCase, "phone image element being compared ", expectedElement, actual.isDisplayed());


	}



	/**
	 * @param testCase
	 * @return the SupportPageObject 
	 * @throws InterruptedException
	 */
	public SupportPage navToNavBarItem(TestCase testCase) throws InterruptedException {
		//navigation code here
		sleep(5000);
		select(driver,By.xpath("menuLocator"));
		_wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(testCase.getParam("navTo"))));
		click(By.xpath(testCase.getParam("navTo")));
		return new SupportPage(driver);
	}

	/**
	 * Close browser.
	 *
	 * @throws Exception the exception
	 */
	protected void closeBrowser() throws Exception {
		log.info("Close browser");
		driver.close();
	}




}












