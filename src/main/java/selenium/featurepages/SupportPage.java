package selenium.featurepages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.bo.Result;
import utils.bo.TestCase;
import selenium.base.BaseSeleniumWebPage;

public class SupportPage extends BaseSeleniumWebPage{

	protected String xpath;
	protected  WebDriverWait _wait;
	protected WebDriver driver;
	private By oncorrectPageLocator=By.xpath("//h1[contains(text(),'FAQs')]");

	public SupportPage(final WebDriver driver) {
		super(driver);
		this.driver = super.getDriver();
		this._wait = super.get_wait();
	}
	
	
	
	
	public void validateElements(final TestCase testCase) {
		final Result expectedResult = testCase.getResultByType("validateElements");
		final boolean expectedElement = expectedResult.getBooleanEntry("correctPageLocator");

		final WebElement actualElement = driver.findElement(oncorrectPageLocator);

		log.info("validate if correct page is loaded ");
		// compare expected and actual
		compareObjectValue(testCase, "correct page is loaded is being compared ", expectedElement, actualElement.isDisplayed());


	}

	/**
	 * Close browser.
	 *
	 * @throws Exception the exception
	 */
	protected void closeBrowser() throws Exception {
		log.info("Close browser");
		driver.close();
	}




}












