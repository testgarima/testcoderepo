package selenium.tests;



import java.time.temporal.ChronoUnit;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import selenium.base.BaseSeleniumTest;
import selenium.featurepages.ConichiMainPage;
import selenium.featurepages.SupportPage;
import utils.bo.TestCase;
import utils.main.TestScript;


/**
 * @author GACH2
 * see @Test method for test implementation
 * uses SupportPage.java
 *
 */
public class SupportPageTest extends BaseSeleniumTest {
	
    protected Logger log = LoggerFactory.getLogger(this.getClass());


	
	 
	@Override
    @BeforeClass(alwaysRun = true)
    @Parameters("testDataFile")
    protected void setUp(@Optional() final String testDataFile) {
        super.setUp(testDataFile);
    }

    /**
     * @throws Exception
     * This Test loads the Conichi main page and clicks on support page link from menu and validate elements which are mentioned in xml under result tag
     * validation logic is implemented Page class(selenium.featurepages.ConichiMainPage)
     * @throws Exception
     */
    @Test
    public void getSupportPage() throws Exception {
        executeForEveryTestCase(new TestScript() {

            @Override
            public void execute(final TestCase testCase) throws Exception {

                final WebDriver driver = openBaseUrl(testCase.getParam("baseURL"));

                final String modifyUserAgent = testCase.getParam("modifyUserAgent");
                if (StringUtils.isNotEmpty(modifyUserAgent)) {
                    changeHeaderChrome(driver, "User-Agent", modifyUserAgent);
                }
                
                final ConichiMainPage mainpage = getMainpage(driver);
               final SupportPage otherPage = (SupportPage) mainpage.navToNavBarItem(testCase);
               driverSleep(3000, ChronoUnit.MILLIS);

               otherPage.validateElements(testCase);
               
                driverSleep(250, ChronoUnit.MILLIS);
                driver.navigate().to(testCase.getParam("baseURL"));
            }
        });
    }
   
}
