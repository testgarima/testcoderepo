package selenium.tests;



import java.time.temporal.ChronoUnit;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import selenium.base.BaseSeleniumTest;
import selenium.featurepages.ConichiMainPage;
import selenium.featurepages.SupportPage;
import utils.bo.TestCase;
import utils.main.TestScript;
import utils.main.common;


/**
 * @author GACH2
 *
 */

public class ConichiMainPageTest extends BaseSeleniumTest {
	
	
    protected Logger log = LoggerFactory.getLogger(this.getClass());


	
	
/* (non-Javadoc)
 * @see selenium.base.BaseSeleniumTest#setUp(java.lang.String)
 */
@Override
    @BeforeClass(alwaysRun = true)
    @Parameters("testDataFile")
    protected void setUp(@Optional() final String testDataFile) {
        super.setUp(testDataFile);
    }

    /**
     * This Test loads the Conichi main page and validate elements which are mentioned in xml under result tag
     * validation logic is implemented Page class(selenium.featurepages.ConichiMainPage)
     * @throws Exception
     */
    @Test(priority=0)
    public void mainPageTestCookieNotification() throws Exception {
        executeForEveryTestCase(new TestScript() {

            @Override
            public void execute(final TestCase testCase) throws Exception {

                final WebDriver driver = openBaseUrl(testCase.getParam("baseURL"));
                log.info("logging");

                final String modifyUserAgent = testCase.getParam("modifyUserAgent");
                if (StringUtils.isNotEmpty(modifyUserAgent)) {
                    changeHeaderChrome(driver, "User-Agent", modifyUserAgent);
                }

                log.info("conichimainpage is loaded ");

                final ConichiMainPage mainpage = getMainpage(driver);
                log.info("conichimainpage is loaded ");

                mainpage.validateElements(testCase);
                
                driverSleep(250, ChronoUnit.MILLIS);
                driver.navigate().to(testCase.getParam("baseURL"));
            }
        });
    }
    
    
   
    
    
    
    
}
