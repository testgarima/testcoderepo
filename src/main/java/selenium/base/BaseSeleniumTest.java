package selenium.base;

import java.io.File;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Sleeper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import selenium.featurepages.ConichiMainPage;
import utils.bo.ExtParam;
import utils.bo.TestData;
import utils.main.common;



public abstract class BaseSeleniumTest extends common {

    protected WebDriver driver = null;

    protected String browser;

    private int browserWidth = 1280;

    private int browserHeight = 768;


    /* (non-Javadoc)
     * @see utils.main.common#setUp(java.lang.String)
     */
    @Override
    @BeforeClass
    @Parameters({ "testDataFile" })
    protected void setUp(@Optional("") final String testDataFile) {
        super.setUp(testDataFile);

        if (System.getProperty("browser") != null) {
            this.browser = System.getProperty("browser");
        } else {
            this.browser = testData.getParam("browser");
        }

        final List<ExtParam> browserExtensionList = testData.getExtParams("browser-extension");
        List<String> browserExtensions = null;
        if (browserExtensionList != null) {
            browserExtensions = new ArrayList<>();
            for (final ExtParam extParam : browserExtensionList) {
                final String extension = extParam.getName();
                browserExtensions.add(extension);
            }
        }
        initDriver(browser, testData.getParam("browserDefaultLanguage"), browserExtensions, testData);

        final Integer width = testData.getIntegerParamOrNull("browserWidth");
        if (width != null) {
            this.browserWidth = width;
        }

        final Integer height = testData.getIntegerParamOrNull("browserHeight");
        if (height != null) {
            this.browserHeight = height;
        }
    }

    @Override
    @AfterClass(alwaysRun = true)
    protected void tearDown() throws Exception {
        super.tearDown();

        driver.quit();
        driver = null;
    }

    /**
     * Close browser.
     *
     * @throws Exception the exception
     */
    protected void closeBrowser() throws Exception {
        logAsInfoHeadlineStars("Close browser");
        driver.quit();
        driver = null;
    }

    /**
     * Initialize the selenium driver
     *
     * @param browser - the browser to be used
     * @param browserDefaultLanguage - default language of the browser
     */
    private void initDriver(final String browser, final String browserDefaultLanguage, final List<String> extensionList, final TestData testData) {

        final String os = System.getProperty("os.name");
        final String userDirectory = System.getProperty("user.dir");
        log.info("OperatingSystem: " + os);
        log.info("UserDirectory: " + userDirectory);
        TestData browserDriver = null;
        try {
            browserDriver = TestData.getInstance("BrowserDriver.xml");
        } catch (final Exception e) {
            log.error("Can't open config. " + e.getMessage());
            // e.printStackTrace();
        }

        switch (browser) {
            // TODO  more browsers

            case "chrome":
                final ChromeOptions chromeOptions = new ChromeOptions();
                final StringBuilder chromeDriverPath = new StringBuilder();
                chromeDriverPath.append(userDirectory);
                chromeDriverPath.append(
                        File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "chromedriver" + File.separator);

                if (os.contains("Linux")) {
                    chromeDriverPath.append("linux64" + File.separator + "chromedriver");
                    chromeOptions.setBinary(browserDriver.getParam("Linux.ChromeBinaryPath"));
                } else if (os.contains("Windows")) {
                    chromeDriverPath.append("win32" + File.separator + "chromedriver.exe");
                    chromeOptions.setBinary(browserDriver.getParam("Windows.ChromeBinaryPath"));
                } else if (os.contains("Mac OS")) {
                    chromeDriverPath.append("mac64" + File.separator + "chromedriver");
                    chromeOptions.setBinary(browserDriver.getParam("Mac.ChromeBinaryPath"));
                } else {
                    writeToErrorSummary(null, "No implementation found for Operating System: " + os);
                    return;
                }

                System.setProperty("webdriver.chrome.driver", chromeDriverPath.toString());
                chromeOptions.addArguments("--lang=" + browserDefaultLanguage);
                // chromeOptions.setCapability("chrome.switches", Arrays.asList("--ignore-certificate-errors"));
                // chromeOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);


                boolean containsModifyHeaders = false;
                if (extensionList != null && !extensionList.isEmpty()) {
                    for (final String extensionName : extensionList) {
                        final File extension = new File("src" + File.separator + "main" + File.separator + "resources" + File.separator + "browserExtensions" + File.separator + extensionName);
                        chromeOptions.addExtensions(extension);
                        log.info(browser + " add extension: " + extensionName);
                        if (extensionName.contains("ModifyHeaders")) {
                            containsModifyHeaders = true;
                        }
                    }
                }

                try {
                    driver = new ChromeDriver(chromeOptions);

                    if (extensionList != null && containsModifyHeaders) {
                        driver = modifyHeadersChrome(testData, driver);
                    }

                } catch (final Exception e) {
                    writeToErrorSummary(null, e.getMessage());
                }
                break;

            case "firefox":
            default:
                final StringBuilder geckoDriverPath = new StringBuilder();

                geckoDriverPath.append("src" + File.separator + "main" + File.separator + "resources" + File.separator + "geckodriver" + File.separator);

                if (os.contains("Linux")) {
                    geckoDriverPath.append("linux64" + File.separator + "geckodriver");
                } else if (os.contains("Windows")) {
                    geckoDriverPath.append("win64" + File.separator + "geckodriver.exe");
                } else if (os.contains("Mac OS")) {
                    geckoDriverPath.append("mac64" + File.separator + "geckodriver");
                } else {
                    writeToErrorSummary(null, "No implementation found for Operating System: " + os);
                    return;
                }
                System.setProperty("webdriver.gecko.driver", geckoDriverPath.toString());

                final FirefoxProfile profile = new FirefoxProfile();
                profile.setPreference("intl.accept_languages", browserDefaultLanguage);
                profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/zip,application/octet-stream");
                profile.setPreference("browser.download.manager.showWhenStarting", false);
                profile.setPreference("browser.tabs.remote.autostart.2", false);
                profile.setPreference("javascript.enabled", true);

                if (extensionList != null && !extensionList.isEmpty()) {
                    for (final String extensionName : extensionList) {
                        final File extension = new File("src" + File.separator + "main" + File.separator + "resources" + File.separator + "browserExtensions" + File.separator + extensionName);
                        profile.addExtension(extension);
                        log.info(browser + " add extension: " + extensionName);
                        // if (extensionName.contains("ModifyHeaders")) {
                        //    profile = modifyHeadersFirefox(testData, profile);
                        //}
                    }
                }


                final FirefoxOptions options = new FirefoxOptions();
                options.addPreference("marionette", true);
                options.addPreference("marionette.logging", "ERROR");
                options.setCapability(FirefoxDriver.PROFILE, profile);

                try {
                    driver = new FirefoxDriver(options);
                } catch (final Exception e) {
                    writeToErrorSummary(null, e.getMessage());
                }
                break;
        }

    }


    /**
     * @param baseUrl
     * @return
     * @throws SeleniumToolException
     */
    public WebDriver openBaseUrl(final String baseUrl) throws SeleniumToolException {

        if (driver == null) {
            writeToErrorSummary(null, "Driver not initialized.");
            return null;
        }

        driver.get(baseUrl);

        // if (driver.getClass().getName().contains("ChromeDriver")) {
        setBrowserSize(browserWidth, browserHeight);
        //} else {
        //driver.manage().window().maximize();
        // }

        if (baseUrl.equals(driver.getCurrentUrl())) {
            log.info("Driver '" + driver.getClass().getName() + "' successfully opened url '" + baseUrl + "' with browser '" + browser + "'");
        } else {

            if (driver.getCurrentUrl() != null) {
                log.warn("Driver '" + driver.getClass().getName() + "' opened url '" + driver.getCurrentUrl() + " instead of requested baseUrl " + baseUrl
                        + "' with browser '" + browser + "'");
            } else {

                throw new SeleniumToolException(
                        "Driver '" + driver.getClass().getName() + "' was unable to open url '" + baseUrl + "' with browser '" + browser + "'");
            }
        }

        return driver;
    }

    /**
     * change the size of the browser
     *
     * @param width - new width
     * @param height - new height
     */
    public void setBrowserSize(final int width, final int height) {
        driver.manage().window().setSize(new Dimension(width, height));
    }

    /**
     * get the current Selenium web driver
     *
     * @return WebDriver
     */
    public WebDriver getDriver() {
        return driver;
    }

    /**
     * @return
     */
    public String getBrowser() {
        return browser;
    }

    /**
     * Uses the Selenium Sleeper and catches exceptions.
     *
     * @param amount
     * @param timeUnit
     */
    public void driverSleep(final long amount, final TemporalUnit timeUnit) {

        try {
            Sleeper.SYSTEM_SLEEPER.sleep(Duration.of(amount, timeUnit));
            log.info("driversleep " + String.valueOf(amount) + " " + timeUnit.toString());
        } catch (final InterruptedException e) {
            e.printStackTrace();
        }
    }

    //    private FirefoxProfile modifyHeadersFirefox(final TestData testData, final FirefoxProfile profile) {
    //        profile.setPreference("modifyheaders.config.active", true);
    //        profile.setPreference("modifyheaders.config.alwaysOn", true);
    //        profile.setPreference("modifyheaders.config.openNewTab", true);
    //        profile.setPreference("modifyheaders.start", true);
    //
    //        final List<ExtParamType> headerList = testData.getExtParams("request-header");
    //        if (headerList != null && headerList.size() > 0) {
    //            profile.setPreference("modifyheaders.headers.count", headerList.size());
    //
    //            final int headerNumber = 0;
    //            for (final ExtParamType headerParams : headerList) {
    //                final String headerAction = headerParams.getParam("headerAction");
    //                final String headerName = headerParams.getParam("headerName");
    //                final String headerValue = headerParams.getParam("headerValue");
    //                final String headerDescription = headerParams.getParam("headerDescription");
    //
    //                profile.setPreference("modifyheaders.headers.action" + String.valueOf(headerNumber), headerAction);
    //                profile.setPreference("modifyheaders.headers.name" + String.valueOf(headerNumber), headerName);
    //                profile.setPreference("modifyheaders.headers.value" + String.valueOf(headerNumber),headerValue);
    //                if (StringUtils.isNotEmpty(headerDescription)) {
    //                    profile.setPreference("modifyheaders.headers.description" + String.valueOf(headerNumber),headerDescription);
    //                }
    //                profile.setPreference("modifyheaders.headers.enabled" + String.valueOf(headerNumber), true);
    //            }
    //        }
    //        return profile;
    //    }

    private WebDriver modifyHeadersChrome(final TestData testData, final WebDriver chromedriver) throws InterruptedException {
        chromedriver.get("chrome-extension://innpjfdalfhpcoinfnehdnbkglpmogdi/options.html");
        chromedriver.navigate().refresh();
        driverSleep(500, ChronoUnit.MILLIS);

        final List<ExtParam> headerList = testData.getExtParams("request-header");
        if (headerList != null && headerList.size() > 0) {
            int headerNumber = 1;
            for (final ExtParam headerParams : headerList) {

                chromedriver.findElement(By.xpath("//button[@tooltip='Add New']")).click();

                final String headerAction = headerParams.getParam("headerAction");
                final String headerName = headerParams.getParam("headerName");
                final String headerValue = headerParams.getParam("headerValue");
                final String headerDescription = headerParams.getParam("headerDescription");

                final WebElement actionElem = chromedriver.findElement(By.name("action"));
                final Select select = new Select(actionElem);
                select.selectByVisibleText(headerAction);

                final WebElement nameElem = chromedriver.findElement(By.name("name"));
                nameElem.sendKeys(headerName);

                final WebElement valueElem = chromedriver.findElement(By.name("value"));
                valueElem.sendKeys(headerValue);

                if (StringUtils.isNotEmpty(headerDescription)) {
                    final WebElement descElem = chromedriver.findElement(By.name("description"));
                    descElem.sendKeys(headerDescription);
                }

                chromedriver.findElement(By.xpath("//button[@tooltip='Save' and @id='btn_save_" + String.valueOf(headerNumber) + "']")).click();
                driverSleep(500, ChronoUnit.MILLIS);

                if (headerNumber == 1) {
                    chromedriver.findElement(By.xpath("//button[@tooltip='Start Modifying Headers']")).click();
                    driverSleep(500, ChronoUnit.MILLIS);
                }

                chromedriver.findElement(By.xpath("//button[@tooltip='Enable' and @id='btn_enable_" + String.valueOf(headerNumber) + "']")).click();
                driverSleep(500, ChronoUnit.MILLIS);

                headerNumber++;
            }
        }

        return chromedriver;
    }

    /**
     * Change a chrome header that was previously created with <ext-param type="request-header">
     *
     * @param chromedriver - the current driver
     * @param headerName - the name of the header to change
     * @param newValue - the new value of the header
     * @return chromedriver
     */
    public WebDriver changeHeaderChrome(final WebDriver chromedriver, final String headerName, final String newValue) {
        chromedriver.get("chrome-extension://innpjfdalfhpcoinfnehdnbkglpmogdi/options.html");
        chromedriver.navigate().refresh();
        driverSleep(1000, ChronoUnit.MILLIS);

        final String xpathPrefix = "//tbody/tr[@class='ng-scope enabled']/td/span[contains(.,'" + headerName + "')]/parent::td/parent::tr";

        final By headerRowNumberLocator = By.xpath(xpathPrefix + "/td[1]");
        final String headerRowNumber = chromedriver.findElement(headerRowNumberLocator).getText();

        final By editHeaderLocator = By.xpath(xpathPrefix + "/td/div/button[@id='btn_edit_" + headerRowNumber + "']");
        final List<WebElement> headerRow = chromedriver.findElements(editHeaderLocator);

        if (headerRow.size() > 0) {
            chromedriver.findElement(editHeaderLocator).click();

            final WebElement valueElem = chromedriver.findElement(By.name("value"));
            valueElem.clear();
            valueElem.sendKeys(newValue);

            chromedriver.findElement(By.xpath("//button[@tooltip='Save' and @id='btn_save_" + headerRowNumber + "']")).click();
            driverSleep(1000, ChronoUnit.MILLIS);
        }
        chromedriver.navigate().back();
        chromedriver.navigate().refresh();

        return chromedriver;
    }
    
    
    /**
     * get the page of the the qa structure
     *
     * @param Drive
     * @return ConichiMainPage
     * @throws InterruptedException
     */
    protected ConichiMainPage getMainpage(final WebDriver driver) throws InterruptedException {
        return new ConichiMainPage(driver);
    }
}
